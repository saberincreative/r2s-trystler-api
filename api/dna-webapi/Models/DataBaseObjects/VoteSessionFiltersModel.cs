﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class VoteSessionFiltersModel
    {
        public long Id { get; set; }
        public long VoteSessionId { get; set; }
        public string KeyWord { get; set; }
        
        public long FilterId { get; set; }

        public Filters Filter { get; set; }

        public VoteSessions VoteSession { get; set; }
    }
}
