﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Filters
    {
        public long Id { get; set; }

        public VoteType VoteType { get; set; }

        public string KeyWord { get; set; }

        public Guid? UserId { get; set; }

        public Users User { get; set; }

        public bool IsDefault { get; set; }

        public FilterType FilterType { get; set; }
        public virtual IEnumerable<VoteSessionFiltersModel> VoteSessionFilters { get; set; }
    }
}
