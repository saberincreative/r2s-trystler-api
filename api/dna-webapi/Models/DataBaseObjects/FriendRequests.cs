﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class FriendRequests
{
        public long Id { get; set; }
        public Guid SenderId { get; set; }
        public Users Sender { get; set; }
        public Guid? RecipientId { get; set; }
        public Users Recipient { get; set; }
        public Guid ShareKey { get; set; }
    }
}
