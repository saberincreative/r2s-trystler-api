﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Items
{
        public long Id { get; set; }
        public string Text { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        //public long Votes { get; set; }

        public string ItemId { get; set; }
        public VoteType VoteType { get; set; }
        public IEnumerable<Votes> Votes { get; set; }
    }
}
