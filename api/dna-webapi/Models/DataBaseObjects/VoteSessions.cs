﻿using DateNightPersistence.Enums;
using DateNightPersistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class VoteSessions
{
        public string Title { get; set; }
        public long Id { get; set; }
        public Guid AUserId { get; set; }

        public Guid BUserId { get; set; }

        public Users UserA { get; set; }

        public Users UserB { get; set; }

        public IEnumerable<Votes> Votes { get; set; }
        public IEnumerable<VoteSessionFiltersModel> VoteSessionFilters { get; set; }
        public IEnumerable<Filters> Filters { get; set; }

        public VoteSessionStatus voteSessionStatus { get; set; }
        public int OptionReduction { get; set; }

        public string SearchTerms { get; set; }

        public string OriginalSearchTerms { get; set; }

        public string FinalizedItemIds { get; set; }

        public string Location { get; set; }
        public DateTime ScheduledDateTime { get; set; }

        public bool isDateTimeStrict { get; set; }
    }
}
