﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Users
{
        public Guid UserId { get; set; }

        public bool? Active { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public IEnumerable<Votes> Votes { get; set; }

        public IEnumerable<Friends> Friends { get; set; }

        public IEnumerable<FriendRequests> FriendRequests { get; set; }

        public IEnumerable<Notifications> Notifications { get; set; }

        public IEnumerable<VoteSessions> VoteSessions { get; set; }
        public IEnumerable<Filters> Filters { get; set; }
    }
}
