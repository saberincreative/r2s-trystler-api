﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Notifications
{
        public long Id { get; set; }

        public Guid UserId { get; set; }

        public string Message { get; set; }

        public NotificationType NotificationType { get; set; }

        public NotificationStatus NotificationStatus { get; set; }

        public TableType TableType { get; set; }

        public string RecordId { get; set; }

        public Users User { get; set; }
    }
}
