﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Friends
{
        public long Id { get; set; }
        public Users User  { get; set; }

        public Guid UserId { get; set; }

        public Users UserFriend { get; set; }

        public string UserFriendIdentifier { get; set; }

        public Guid? UserFriendId { get; set; }

        public FriendStatus FriendStatus { get; set; }

        public string? FriendCode { get; set; }
    }
}
