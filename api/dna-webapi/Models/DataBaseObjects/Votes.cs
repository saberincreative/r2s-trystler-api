﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models.DataBaseObjects
{
    public class Votes
    {
        public long Id { get; set; }
        public string ItemId { get; set; }

        public VoteType VoteType { get; set; }
        public Guid UserId { get; set; }

       
        public long VoteSessionId { get; set; }

        public bool IsAffirmative { get; set; }
        public bool IsNoVote { get; set; }

        public VoteStatus VoteStatus { get; set; }

        public Items Item { get; set; }

        public  Users User { get; set; }

        
        public  VoteSessions VoteSession { get; set; }

        public bool VotedOn { get; set; }
    }
}
