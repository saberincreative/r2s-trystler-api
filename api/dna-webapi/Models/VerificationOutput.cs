﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Models
{
    public class VerificationOutput
{
        public string Error { get; set; }

        public bool Success { get; set; }
    }
}
