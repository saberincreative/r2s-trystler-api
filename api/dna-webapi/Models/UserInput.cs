﻿namespace DateNightApi.Models
{
    public class UserInput
    {
        public string extension_3b85de49fa6f49c6a059d0600a8a08e5_PhoneNumber { 
            set
            {
                if (value is not null)
                {
                    PhoneNumber = value;
                }
            } 
        }

        public string extension_8c28cabde0e840ebb937213520eaad27_PhoneNumber
        {
            set
            {
                if (value is not null)
                {
                    PhoneNumber = value;
                }
            }
        }

        public string Surname { get; set; }

        public string GivenName { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string PhoneNumber { get; set; }
    }
}
