using DateNightApi.Options;
using DateNightApi.Services;
using DateNightDataServices;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authentication;

using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.OpenApi.Models;
using Saberin.SerilogEnricher;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DateNightApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
               .AddJsonFile(configuration["SecretsLocation"], optional: false, reloadOnChange: true)
               .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.

                options.Secure = CookieSecurePolicy.Always;
            });

          

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddMicrosoftIdentityWebApi(Configuration, "AzureAd").EnableTokenAcquisitionToCallDownstreamApi()
                .AddInMemoryTokenCaches(); 

            services.Configure<WebApiConfig>(Configuration.GetSection("WebApiSettings"));
            services.Configure<GraphApiConfig>(Configuration.GetSection("GraphApi"));

            services.AddAuthorization(options =>
            {

                options.FallbackPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
            });
           
            services.AddControllers(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DateNightApi", Version = "v1" });
            });

          



            services.AddDbContext<DateNightPersistence.DataContext>(options =>
                options
                    .UseLazyLoadingProxies()
                    .UseSqlServer(Configuration.GetConnectionString("connection_string")));
            services.AddApplicationInsightsTelemetry(options => {
                options.EnableDependencyTrackingTelemetryModule = false;
                options.DeveloperMode = false;
                options.EnableActiveTelemetryConfigurationSetup = true;
                
            });
            services.AddScoped<GraphService>();
            services.AddScoped<DateNightDataServices.UserService>();
            services.AddScoped<DateNightDataServices.ItemService>();
            services.AddScoped<DateNightDataServices.VoteSessionService>();
            services.AddScoped<DateNightDataServices.VoteService>();
            services.AddScoped<DateNightDataServices.FriendService>();
            services.AddScoped<DateNightDataServices.FriendRequestService>();
            services.AddScoped<DateNightApi.Services.AzureService>();
            services.AddScoped<DateNightApi.Services.HangfireService>();
            services.AddScoped<DateNightDataServices.NotificationService>();
            services.AddScoped<DateNightDataServices.VoteSessionFilterService>();
            services.AddScoped<DateNightDataServices.FilterService>();
            services.AddScoped<DateNightDataServices.SignalRService>();
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("connection_string"), new SqlServerStorageOptions
                {
                    SchemaName = "Hangfire-Trystler",
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                })
                );
            services.AddControllers()
                    .AddJsonOptions(c => { 
                        c.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve; 
                        c.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                        c.JsonSerializerOptions.MaxDepth = 0;
                    });
            services.AddHangfireServer();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, ILogger<Startup> logger, DateNightPersistence.DataContext dataContext, IHttpContextAccessor httpContextAccessor)
        {
            dataContext.Database.Migrate();
            ConfigureSeriLog(httpContextAccessor);
            loggerFactory.AddSerilog();
            app.UseSerilogRequestLogging();

            //loggerFactory.AddLog4Net();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DateNightApi v1"));
            }
            else
            {
                app.UseHttpsRedirection();
            }
            
            RecurringJob.AddOrUpdate(() => 
                new HangfireService(loggerFactory.CreateLogger<HangfireService>(), 
                app.ApplicationServices.CreateScope().ServiceProvider.GetService<VoteSessionService>()).ExpireVoteSessions(), 
                "*/5 * * * *"
            );

            app.UseCookiePolicy();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //app.Use((context, next) =>
            //{
            //    context.Request.Scheme = "http";
            //    return next();
            //});
        }
        private void ConfigureSeriLog(IHttpContextAccessor httpContextAccessor)
        {
            //Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));

            //Log.Logger = new LoggerConfiguration()
            //        .ReadFrom.Configuration(Configuration)
            //        .Enrich.FromLogContext()
            //        .Enrich.With(new WebApplicationEnricher(httpContextAccessor))
            //        .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", Serilog.Events.LogEventLevel.Warning)
            //        .MinimumLevel.Verbose()
            //        .CreateLogger();


            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    // Filter out ASP.NET Core infrastructre logs that are Information and below
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .Enrich.With(new WebApplicationEnricher(httpContextAccessor))
                .CreateLogger();




            Log.Information("Serilog configured");
            Log.Error("Exception Error");
            Log.Warning("Warning");

        }
    }
}