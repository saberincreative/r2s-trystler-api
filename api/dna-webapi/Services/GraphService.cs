﻿using Azure.Identity;
using DateNightApi.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Services
{
    public class GraphService
    {
        private readonly ILogger<GraphService> _logger;
        private readonly IOptions<GraphApiConfig> _graphApiConfig;
        private readonly GraphServiceClient _client;
        public GraphService(ILogger<GraphService> logger, IOptions<GraphApiConfig> graphApiConfig)
        {
            _logger = logger;
            _graphApiConfig = graphApiConfig;
            var auth = new ClientSecretCredential(
             graphApiConfig.Value.TenantId, graphApiConfig.Value.ClientId, graphApiConfig.Value.ClientSecret);

            _client = new GraphServiceClient(auth);
        }
        public async Task<List<User>> ListGraphUsers()
        {
            List<User> allUsers = new List<User>();

            IGraphServiceUsersCollectionPage users = await _client.Users.Request().GetAsync();

            while (users.Count > 0)
            {
                allUsers.AddRange(users);
                if (users.NextPageRequest != null)
                {
                    users = await users.NextPageRequest.GetAsync();
                }
                else
                {
                    break;
                }
            }

            return allUsers;
        }

        public async Task<bool> DeleteUserAsync(string oid)
        {
            try
            {
                await _client.Users[oid].Request().DeleteAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "DeleteUser error");
                return false;
            }
        }

        public async Task<bool> IsAccountEnabled(string oid)
        {
            var appId = _graphApiConfig.Value.ExtensionAppId.Replace("-", "");
            if (string.IsNullOrEmpty(oid))
            {
                _logger.LogError("Object Id is not present");
                return true;
            }
            try
            {
                _logger.LogInformation($"Checking if account enabled for User with id {oid}");
                var accountEnabled = await _client.Users[oid].Extensions[$"extension_{appId}_AccountEnabled"].Request().GetAsync();
                var val = accountEnabled.AdditionalData["AccountEnabled"].ToString();
                bool output;
                bool.TryParse(val, out output);
                return output;
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex, "Failed to get value associated with Extension. Most likely due to extension not existing for user in question.");
                _logger.LogWarning(ex, "Since no extension exists, return default of true");
                return true;
            }

        }
        public async Task ModifyExtensionAccountEnabled(bool isAccountEnabled, string oid)
        {
            if (string.IsNullOrEmpty(oid))
            {
                _logger.LogError("Object Id is not present");
                return;
            }
            var appId = _graphApiConfig.Value.ExtensionAppId.Replace("-", "");
            var extension = new OpenTypeExtension
            {
                ExtensionName = $"extension_{appId}_AccountEnabled",
                AdditionalData = new Dictionary<string, object>
                 {
                     {"AccountEnabled", isAccountEnabled},
                 }
            };
            try
            {
                _logger.LogInformation($"Checking if extension exists for user with id {oid}");
                await IsAccountEnabled(oid);
                _logger.LogInformation("Attempting to modify extension value");
                await _client.Users[oid].Extensions[$"extension_{appId}_AccountEnabled"].Request().UpdateAsync(extension);
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Extension in question does not exist. Adding extension instead");
                await _client.Users[oid].Extensions
               .Request()
               .AddAsync(extension);
            }
          
           

        }
    }
}
