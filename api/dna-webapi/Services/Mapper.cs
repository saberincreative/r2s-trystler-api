﻿using DateNightApi.Models.DataBaseObjects;
using DateNightPersistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Services
{
    public static class Mapper
    {
        #region Item->Items
        /*
        public static IEnumerable<Items> Map(IEnumerable<Item> from, IEnumerable<Items> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(i => new Items
                {
                    Id = i.Id,
                    Text = i.Text,
                    Description = i.Description,
                    ImageUrl = i.ImageUrl,
                    ItemId = i.ItemId,
                    VoteType = i.VoteType,
                    Votes = i.Votes?.OrderByDescending(v=> v.Id).Take(5).Select(v => new Votes {
                        Id = v.Id,
                        ItemId = v.ItemId,
                        UserId = v.UserId,
                        VoteSessionId = v.VoteSessionId,
                        IsAffirmative = v.IsAffirmative,
                        IsNoVote = v.IsNoVote,
                        VoteStatus = v.VoteStatus,
                        VotedOn = v.VotedOn
                    }).AsEnumerable()

                }).AsQueryable();
            }
            return to;
        }

        public static Items Map(Item from, Items to) {
            if (from == null) return null;

            var fromList = new List<Item>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Items>()).AsQueryable()).First();
        }
        */
        #endregion

        #region Vote->Votes
        public static IEnumerable<Votes> Map(IEnumerable<Vote> from, IEnumerable<Votes> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(v => new Votes
                {
                    Id = v.Id,
                    ItemId = v.ItemId,
                    UserId = v.UserId,
                    VoteSessionId = v.VoteSessionId,
                    IsAffirmative = v.IsAffirmative,
                    IsNoVote = v.IsNoVote,
                    VoteStatus = v.VoteStatus,
                    VotedOn = v.VotedOn,
                    VoteType = v.VoteType,
                    User = v.User == null? null : new Users { 
                        UserId = v.User.UserId,
                        Active = v.User.Active,
                        DisplayName = v.User.DisplayName,
                        FirstName = v.User.FirstName,
                        LastName=  v.User.LastName,
                        Email = v.User.Email,
                        PhoneNumber = v.User.PhoneNumber

                    },
                    VoteSession = v.VoteSession == null? null : new VoteSessions { 
                        Id = v.VoteSession.Id,
                        AUserId = v.VoteSession.AUserId,
                        BUserId = v.VoteSession.BUserId,
                        voteSessionStatus = v.VoteSession.voteSessionStatus,
                        OptionReduction = v.VoteSession.OptionReduction,
                        SearchTerms = v.VoteSession.SearchTerms,
                        OriginalSearchTerms = v.VoteSession.OriginalSearchTerms,
                        FinalizedItemIds = v.VoteSession.FinalizedItemIds
                    }

                }).AsQueryable();
            }
            return to;
        }

        public static Votes Map(Vote from, Votes to)
        {
            if (from == null) return null;

            var fromList = new List<Vote>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Votes>()).AsQueryable()).First();
        }
        #endregion

        #region VoteSession->VoteSessions
        public static IEnumerable<VoteSessions> Map(IEnumerable<VoteSession> from, IEnumerable<VoteSessions> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(vs => new VoteSessions
                {
                    Title = vs.Title,
                    Id = vs.Id,
                    AUserId = vs.AUserId,
                    BUserId = vs.BUserId,
                    isDateTimeStrict = vs.isDateTimeStrict,
                    ScheduledDateTime = vs.ScheduledDateTime,
                    UserA = vs.UserA == null ? null : new Users
                    {
                        UserId = vs.UserA.UserId,
                        Active = vs.UserA.Active,
                        DisplayName = vs.UserA.DisplayName,
                        FirstName = vs.UserA.FirstName,
                        LastName = vs.UserA.LastName,
                        Email = vs.UserA.Email,
                        PhoneNumber = vs.UserA.PhoneNumber

                    },
                    UserB = vs.UserB == null ? null : new Users
                    {
                        UserId = vs.UserB.UserId,
                        Active = vs.UserB.Active,
                        DisplayName = vs.UserB.DisplayName,
                        FirstName = vs.UserB.FirstName,
                        LastName = vs.UserB.LastName,
                        Email = vs.UserB.Email,
                        PhoneNumber = vs.UserB.PhoneNumber

                    },
                    Votes = vs.Votes?.Where(v=> v.VotedOn == false).OrderByDescending(v => v.Id).Select(v => new Votes
                    {
                        Id = v.Id,
                        ItemId = v.ItemId,
                        VoteType = v.VoteType,
                        UserId = v.UserId,
                        VoteSessionId = v.VoteSessionId,
                        IsAffirmative = v.IsAffirmative,
                        IsNoVote = v.IsNoVote,
                        VoteStatus = v.VoteStatus,
                        VotedOn = v.VotedOn
                    }).AsEnumerable(),
                    VoteSessionFilters = vs.VoteSessionFilters?
                        .Select(vsf => new Models.DataBaseObjects.VoteSessionFiltersModel { 
                            Id = vsf.Id,
                            VoteSessionId = vsf.VoteSessionId,
                            FilterId = vsf.FilterId,
                            KeyWord = vsf.Filter?.KeyWord,
                        }).AsEnumerable(),
                    Filters = vs.VoteSessionFilters?
                        .Select(vsf => Map(vsf.Filter, new Filters())).AsEnumerable(),
                    voteSessionStatus = vs.voteSessionStatus,
                    OptionReduction = vs.OptionReduction,
                    SearchTerms = vs.SearchTerms,
                    OriginalSearchTerms = vs.OriginalSearchTerms,
                    FinalizedItemIds = vs.FinalizedItemIds,
                    Location = vs.Location
                }).AsQueryable();
            }
            return to;
        }

        public static VoteSessions Map(VoteSession from, VoteSessions to)
        {
            if (from == null) return null;

            var fromList = new List<VoteSession>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<VoteSessions>()).AsQueryable()).First();
        }
        #endregion

        #region Notification->Notifications
        public static IEnumerable<Notifications> Map(IEnumerable<Notification> from, IEnumerable<Notifications> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(n => new Notifications
                {
                   Id = n.Id,
                   UserId = n.UserId,
                   Message = n.Message,
                   NotificationStatus = n.NotificationStatus,
                   NotificationType = n.NotificationType,
                   TableType = n.TableType,
                   RecordId = n.RecordId,
                   User = n.User == null ? null : new Users
                    {
                        UserId = n.User.UserId,
                        Active = n.User.Active,
                        DisplayName = n.User.DisplayName,
                        FirstName = n.User.FirstName,
                        LastName = n.User.LastName,
                        Email = n.User.Email,
                        PhoneNumber = n.User.PhoneNumber

                    },
                }).AsQueryable();
            }
            return to;
        }

        public static Notifications Map(Notification from, Notifications to)
        {
            if (from == null) return null;

            var fromList = new List<Notification>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Notifications>()).AsQueryable()).First();
        }
        #endregion

        #region FriendRequest->FriendRequests
        public static IEnumerable<FriendRequests> Map(IEnumerable<FriendRequest> from, IEnumerable<FriendRequests> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(fr => new FriendRequests
                {
                    Id = fr.Id,
                    SenderId = fr.SenderId,
                    RecipientId = fr.RecipientId,
                    ShareKey = fr.ShareKey,
                    Sender = fr.Sender == null ? null : new Users
                    {
                        UserId = fr.Sender.UserId,
                        Active = fr.Sender.Active,
                        DisplayName = fr.Sender.DisplayName,
                        FirstName = fr.Sender.FirstName,
                        LastName = fr.Sender.LastName,
                        Email = fr.Sender.Email,
                        PhoneNumber = fr.Sender.PhoneNumber

                    },
                    Recipient = fr.Recipient == null ? null : new Users
                    {
                        UserId = fr.Recipient.UserId,
                        Active = fr.Recipient.Active,
                        DisplayName = fr.Recipient.DisplayName,
                        FirstName = fr.Recipient.FirstName,
                        LastName = fr.Recipient.LastName,
                        Email = fr.Recipient.Email,
                        PhoneNumber = fr.Recipient.PhoneNumber

                    },
                }).AsQueryable();
            }
            return to;
        }

        public static FriendRequests Map(FriendRequest from, FriendRequests to)
        {
            if (from == null) return null;

            var fromList = new List<FriendRequest>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<FriendRequests>()).AsQueryable()).First();
        }
        #endregion

        #region Friend->Friends
        public static IEnumerable<Friends> Map(IEnumerable<Friend> from, IEnumerable<Friends> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(f => new Friends
                {
                    Id = f.Id,
                    UserId = f.UserId,
                    UserFriendId = f.UserFriendId,
                    UserFriendIdentifier = f.UserFriendIdentifier,
                    FriendStatus = f.FriendStatus,
                    FriendCode = f.FriendCode,

                    User = f.User == null ? null : new Users
                    {
                        UserId = f.User.UserId,
                        Active = f.User.Active,
                        DisplayName = f.User.DisplayName,
                        FirstName = f.User.FirstName,
                        LastName = f.User.LastName,
                        Email = f.User.Email,
                        PhoneNumber = f.User.PhoneNumber

                    },
                    UserFriend = f.UserFriend == null ? null : new Users
                    {
                        UserId = f.UserFriend.UserId,
                        Active = f.UserFriend.Active,
                        DisplayName = f.UserFriend.DisplayName,
                        FirstName = f.UserFriend.FirstName,
                        LastName = f.UserFriend.LastName,
                        Email = f.UserFriend.Email,
                        PhoneNumber = f.UserFriend.PhoneNumber

                    },
                }).AsQueryable();
            }
            return to;
        }

        public static Friends Map(Friend from, Friends to)
        {
            if (from == null) return null;

            var fromList = new List<Friend>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Friends>()).AsQueryable()).First();
        }
        #endregion

        #region User->Users
        public static IEnumerable<Users> Map(IEnumerable<User> from, IEnumerable<Users> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(u => new Users
                {
                    UserId = u.UserId,
                    Active = u.Active,
                    DisplayName  = u.DisplayName,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    PhoneNumber = u.PhoneNumber,
                    Votes = u.Votes?.OrderByDescending(v => v.Id).Take(5).Select(v => new Votes
                    {
                        Id = v.Id,
                        ItemId = v.ItemId,
                        VoteType = v.VoteType,
                        UserId = v.UserId,
                        VoteSessionId = v.VoteSessionId,
                        IsAffirmative = v.IsAffirmative,
                        IsNoVote = v.IsNoVote,
                        VoteStatus = v.VoteStatus,
                        VotedOn = v.VotedOn
                    }).AsEnumerable(),
                    Friends = u.UserAFriend?.Concat(u.UserBFriend)?.OrderByDescending(f=> f.Id).Take(10).Select(f => new Friends {
                        Id = f.Id,
                        UserId = f.UserId,
                        UserFriendId = f.UserFriendId,
                        UserFriendIdentifier = f.UserFriendIdentifier,
                        FriendStatus = f.FriendStatus,
                        FriendCode = f.FriendCode,
                    }).AsEnumerable(),
                    FriendRequests = u.Sender?.Concat(u.Recipient)?.OrderByDescending(fr => fr.Id).Take(10).Select(fr => new FriendRequests {
                        Id = fr.Id,
                        SenderId = fr.SenderId,
                        RecipientId = fr.RecipientId,
                        ShareKey = fr.ShareKey,
                    }).AsEnumerable(),
                    Notifications = u.Notifications?
                        .OrderBy(n => n.NotificationStatus)
                        .Where(n => n.NotificationStatus != DateNightPersistence.Enums.NotificationStatus.Dismissed)
                        .Take(10).Select(n => new Notifications {
                        Id = n.Id,
                        UserId = n.UserId,
                        Message = n.Message,
                        NotificationStatus = n.NotificationStatus,
                        NotificationType = n.NotificationType,
                        TableType = n.TableType,
                        RecordId = n.RecordId,
                    }).AsEnumerable(),
                    VoteSessions = u.UserAVoteSession?.Concat(u.UserBVoteSession)?
                        .OrderByDescending(vs => vs.Id)
                        .Where(vs => vs.voteSessionStatus != DateNightPersistence.Enums.VoteSessionStatus.Complete)
                        .Take(5).Select(vs => new VoteSessions {
                        Id = vs.Id,
                        AUserId = vs.AUserId,
                        BUserId = vs.BUserId,
                        voteSessionStatus = vs.voteSessionStatus,
                        OptionReduction = vs.OptionReduction,
                        SearchTerms = vs.SearchTerms,
                        OriginalSearchTerms = vs.OriginalSearchTerms,
                        FinalizedItemIds = vs.FinalizedItemIds
                    }).AsEnumerable(),
                    Filters = u.Filters?
                        .OrderBy(f => f.IsDefault)
                        .Where(f=> f.IsDefault == true)
                        .Take(1).Select(f => new Filters { 
                            Id = f.Id,
                            VoteType = f.VoteType,
                            KeyWord = f.KeyWord,
                            UserId = f.UserId,
                            IsDefault = f.IsDefault
                        }).AsEnumerable(),

                }).AsQueryable();
            }
            return to;
        }
 
        public static Users Map(User from, Users to)
        {
            if (from == null) return null;

            var fromList = new List<User>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Users>()).AsQueryable()).First();
        }
        #endregion

        #region Filter->Filters
        public static IEnumerable<Filters> Map(IEnumerable<Filter> from, IEnumerable<Filters> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(f => new Filters
                {
                    Id = f.Id,
                    VoteType = f.VoteType,
                    KeyWord = f.KeyWord,
                    UserId = f.UserId,
                    IsDefault = f.IsDefault,
                    FilterType = f.FilterType,
                    User = f.User == null ? null : new Users
                    {
                        UserId = f.User.UserId,
                        Active = f.User.Active,
                        DisplayName = f.User.DisplayName,
                        FirstName = f.User.FirstName,
                        LastName = f.User.LastName,
                        Email = f.User.Email,
                        PhoneNumber = f.User.PhoneNumber

                    },
                    VoteSessionFilters = f.VoteSessionFilters?
                        .Take(5).Select(vsf => new Models.DataBaseObjects.VoteSessionFiltersModel
                        {
                            Id = vsf.Id,
                            VoteSessionId = vsf.VoteSessionId,
                            FilterId = vsf.FilterId,
                        }).AsEnumerable(),
                }).AsQueryable();
            }
            return to;
        }

        public static Filters Map(Filter from, Filters to)
        {
            if (from == null) return null;

            var fromList = new List<Filter>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Filters>()).AsQueryable()).First();
        }
        #endregion

        #region VoteSessionFilters->VoteSessionFilters
        public static IEnumerable<Models.DataBaseObjects.VoteSessionFiltersModel> Map(IEnumerable<DateNightPersistence.Models.VoteSessionFilters> from, IEnumerable<Models.DataBaseObjects.VoteSessionFiltersModel> to)
        {
            if (from == null)
            {
                to = null;
            }
            else
            {
                to = from.Select(vsf => new Models.DataBaseObjects.VoteSessionFiltersModel
                {
                    Id = vsf.Id,
                    VoteSessionId = vsf.VoteSessionId,
                    FilterId = vsf.FilterId,
                    Filter = vsf.Filter == null? null : new Filters {
                        Id = vsf.Filter.Id,
                        VoteType = vsf.Filter.VoteType,
                        KeyWord = vsf.Filter.KeyWord,
                        UserId = vsf.Filter.UserId,
                        IsDefault = vsf.Filter.IsDefault
                    },
                    VoteSession = vsf.VoteSession == null? null : new VoteSessions { 
                        Id = vsf.VoteSession.Id,
                        AUserId = vsf.VoteSession.AUserId,
                        BUserId = vsf.VoteSession.BUserId,
                        voteSessionStatus = vsf.VoteSession.voteSessionStatus,
                        OptionReduction = vsf.VoteSession.OptionReduction,
                        SearchTerms = vsf.VoteSession.SearchTerms,
                        OriginalSearchTerms = vsf.VoteSession.OriginalSearchTerms,
                        FinalizedItemIds = vsf.VoteSession.FinalizedItemIds
                    },
                }).AsQueryable();
            }
            return to;
        }

        public static Models.DataBaseObjects.VoteSessionFiltersModel Map(DateNightPersistence.Models.VoteSessionFilters from, Models.DataBaseObjects.VoteSessionFiltersModel to)
        {
            if (from == null) return null;

            var fromList = new List<DateNightPersistence.Models.VoteSessionFilters>
            {
                from
            };

            return Map(fromList.AsQueryable(), (new List<Models.DataBaseObjects.VoteSessionFiltersModel>()).AsQueryable()).First();
        }
        #endregion
    }
}
