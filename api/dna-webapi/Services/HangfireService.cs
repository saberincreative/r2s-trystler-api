﻿using DateNightDataServices;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Services
{
    public class HangfireService
    {
        private readonly ILogger<HangfireService> _logger;
        private readonly VoteSessionService _voteSessionService;
        

        public HangfireService(ILogger<HangfireService> logger, VoteSessionService voteSessionService) {
            _logger = logger;
            _voteSessionService = voteSessionService;
        }

        public void ExpireVoteSessions() {
            _logger.LogInformation($"Expiring Vote Sessions that are scheduled for before {DateTime.Now} and have not been completed");
            _voteSessionService.ExpireVoteSessions("Hangfire");
        }
    }

}
