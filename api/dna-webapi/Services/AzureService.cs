﻿using DateNightApi.Models;
using DateNightDataServices;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DateNightApi.Services
{
    public class AzureService
    {
        private readonly ILogger<AzureService> _logger;
        private UserService _userService;
        public AzureService(ILogger<AzureService> logger, UserService userService)
        {
            _logger = logger;
            _userService = userService;
            
        }
        public async Task<VerificationOutput> VerifyUserInput(UserInput userInput)
        {
            var returnVal = new VerificationOutput
            {
                Success = true
            };

            try
            {
                if (!string.IsNullOrWhiteSpace(userInput.Email) && _userService.IsExistingUserwithEmail(userInput.Email))
                {
                    returnVal.Success = false;
                    returnVal.Error = "A user already exists with inputted email";
                    _logger.LogError($"Failed to Verify for user signup with DisplayName {userInput.DisplayName}. Error {returnVal.Error}");
                    return returnVal;
                }

                if (string.IsNullOrWhiteSpace(userInput.DisplayName))
                {
                    returnVal.Success = false;
                    returnVal.Error = "Display Name is required";
                    _logger.LogError($"Failed to Verify for user signup with email {userInput.Email}. Error {returnVal.Error}");
                    return returnVal;
                }
                else
                {
                    if (_userService.IsExistingUserwithDisplayName(userInput.DisplayName))
                    {
                        returnVal.Success = false;
                        returnVal.Error = "A user already exists with inputted display name";
                        _logger.LogError($"Failed to Verify for user signup with DisplayName {userInput.DisplayName}. Error {returnVal.Error}");
                        return returnVal;
                    }
                }
                return returnVal;
            } 
            catch (Exception e) {
                returnVal.Success = false;
                returnVal.Error = e.Message;
                _logger.LogError($"Failed to Verify for user signup with DisplayName {userInput.DisplayName}", e);
                return returnVal;
            }
        }
    }
}
