﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Helpers
{
    public class Serialization
    {
        public static object Serialize(object model, int maxDepth = 1, bool returnSimpleObject = true)
        {
            var settings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                MaxDepth = maxDepth
            };
            var json = JsonConvert.SerializeObject(model, settings);
            if (!returnSimpleObject) return json;
            var simpleObject = JsonConvert.DeserializeObject(json);
            return simpleObject;
        }
    }
}
