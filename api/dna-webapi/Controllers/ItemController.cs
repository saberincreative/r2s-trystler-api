﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightDataServices;
using System.Net.Http;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{

    [Route("api/Item")]
    [ApiController]
    [Authorize]
    public class ItemController : ControllerBase
    {
        private readonly ILogger<ItemController> _logger;
        private UserService _userService;
        private ItemService _itemService;

        public ItemController(ILogger<ItemController> logger, UserService userService, ItemService itemService)
        {
            _logger = logger;
            _userService = userService;
            _itemService = itemService;
        }
        #region unusedCode
        ///// <summary>
        ///// The method gets a queryable list of item objects
        ///// </summary>
        ///// <returns>A Queryable list of items</returns>
        //[HttpGet]
        //[Route("Get")]
        //public IEnumerable<Items> Get()
        //{
        //    _logger.LogInformation($"Get item records");
        //    IEnumerable<Item> itemsFromDB = _itemService.GetQueryable().AsEnumerable();
        //    IEnumerable<Items> itemsMapped = Mapper.Map(itemsFromDB, new List<Items>().AsEnumerable());
        //    return itemsMapped;
        //}

        ///// <summary>
        ///// The method takes an item object and tries to insert the item into the database, returning the 
        ///// response to the user
        ///// </summary>
        ///// <param name="newItem">Item Object</param>
        ///// <returns>Response regarding the success of the call</returns>
        //[Route("InsertNewItem")]
        //[HttpPost]
        //public ObjectResult InsertNewItem(Item newItem) {
            
        //    #region variables
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    bool isSuccess = true;
        //    string message = "";
        //    _logger.LogInformation($"Inserting a new item record with itemId {newItem.ItemId}");
        //    var user = _userService.GetQueryable().First();
        //    #endregion
            
        //    _itemService.InsertNewItem(newItem, user, ref isSuccess, ref message);

        //    switch (isSuccess)
        //    {
        //        case true:
        //            return Ok(Mapper.Map(newItem, new Items()));
                    
        //        case false:
        //            return BadRequest(message);
                    
        //    }

        //}
        #endregion

    }
}