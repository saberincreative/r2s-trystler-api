﻿using DateNightApi.Authentication;
using DateNightApi.Models;
using DateNightApi.Services;
using DateNightDataServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;



namespace DateNightApi.Controllers
{
    [Route("api/Azure")]
    [ApiController]
    [AllowAnonymous]
    public class AzureController : ControllerBase
    {
        private readonly ILogger<AzureController> _logger;
        private AzureService _azureService;
        private GraphService _graphService;
        public AzureController(ILogger<AzureController> logger, AzureService azureService, GraphService graphService)
        {
            _logger = logger;
            _azureService = azureService;
            _graphService = graphService;
        }


        [BasicAuth]
        [HttpPost]
        [Route("VerifyUserInput")]
        public async Task<IActionResult> VerifyUserInput([FromBody] UserInput userInput)
        {
            
           var output = await _azureService.VerifyUserInput(userInput);

            if (!output.Success)
            {                
                //return Conflict(new B2CResponse() { UserMessage = output.Error });
                return BadRequest(new ResponseContent("CONTOSOERR001", output.Error, HttpStatusCode.BadRequest, "ValidationError"));
            }

            return Ok(output);
        }

        [HttpGet]
        [Route("TEST")]
        public async Task<IActionResult> getTest() {
            var targetId = "FA8272B4-405F-470F-86BA-902656C8C18E".ToLower();
            var users = await _graphService.ListGraphUsers();
            var enabled = await _graphService.IsAccountEnabled(targetId);
            await _graphService.ModifyExtensionAccountEnabled(!enabled, targetId);
            return Ok(users.First(u => u.Id == targetId));
        }

        //[HttpGet]
        //[Route("TestAzureExceptionLogs")]
        //public async void TestAzureException()
        //{
        //    try { throw new NotImplementedException(); } catch (Exception ex) { _logger.LogError(ex, "Log Error"); }

        //}
    }
}
