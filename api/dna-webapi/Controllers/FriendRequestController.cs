﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Microsoft.Identity.Web.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightPersistence.Enums;
using DateNightDataServices;
using System.Net.Http;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{
    [Route("api/FriendRequest")]
    [ApiController]
    [Authorize]
    public class FriendRequestController : ControllerBase
    {
        private readonly ILogger<FriendRequestController> _logger;
        private FriendService _friendService;
        private UserService _userService;
        private FriendRequestService _friendRequestService;
        private GraphService _graphService;
        public FriendRequestController(ILogger<FriendRequestController> logger, FriendService friendService, UserService userService, FriendRequestService friendRequestService, GraphService graphService)
        {
            _logger = logger;
            _friendService = friendService;
            _userService = userService;
            _friendRequestService = friendRequestService;
            _graphService = graphService;
        }

        /// <summary>
        /// Method takes ina guid and tries to return a friendrequest record with the same sharekey
        /// </summary>
        /// <param name="shareKey">guid share key</param>
        /// <returns></returns>
        [Route("GetFriendRequestRecordByShareKey")]
        [HttpGet]
        public async Task<ObjectResult> GetFriendRequestRecordByShareKey(Guid shareKey, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get friend request record using the key: {shareKey}");
                FriendRequest friendRequest = _friendRequestService.GetFriendRequestRecordByShareKey(shareKey);
                FriendRequests friendRequestMapped = Mapper.Map(friendRequest, new FriendRequests());
                return Ok(friendRequestMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// Method takes in a friendrequest object and tries to insert the record
        /// </summary>
        /// <param name="friendRequest">friend request object</param>
        /// <returns></returns>
        [Route("CreateFriendRequest")]
        [HttpPost]
        public async Task<ObjectResult> CreateFriendRequest(FriendRequest friendRequest, string objectId)
        {

            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Creating friend request for sender {friendRequest.SenderId}");

                friendRequest = _friendRequestService.GetFriendRequest(friendRequest.SenderId);
                friendRequest.Sender = _userService.Get(friendRequest.SenderId.ToString());

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(friendRequest, new FriendRequests()));
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Method takes in a friendrequest object and tries to insert the record
        /// </summary>
        /// <param name="friendRequest">friend request object</param>
        /// <returns></returns>
        [Route("SendFriendRequest")]
        [HttpPost]
        public async Task<ObjectResult> SendFriendRequest(FriendRequest friendRequest, string objectId)
        {

            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Creating friend request for sender {friendRequest.SenderId}, recipient {friendRequest.RecipientId}, share key {friendRequest.ShareKey}");

                friendRequest.Sender = _userService.Get(friendRequest.SenderId.ToString());
                friendRequest.Recipient = _userService.Get(friendRequest.RecipientId.ToString());

                _friendRequestService.InsertFriendRequest(friendRequest, ref isSuccess, ref message);
                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(friendRequest, new FriendRequests()));
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// Method takes in a friendrequest object and if it exists in the db tries to update the record
        /// </summary>
        /// <param name="friendRequest">friend request object</param>
        /// <returns></returns>
        [Route("UpdateFriendRequest")]
        [HttpPost]
        public async Task<ObjectResult> UpdateFriendRequest(FriendRequest friendRequest, string objectId)
        {

            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Updating friend request for sender {friendRequest.SenderId}, recipient {friendRequest.RecipientId}, share key {friendRequest.ShareKey}");

                friendRequest.Sender = _userService.Get(friendRequest.SenderId.ToString());
                friendRequest.Recipient = _userService.Get(friendRequest.RecipientId.ToString());

                _friendRequestService.UpdateFriendRequest(friendRequest, ref isSuccess, ref message);

                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(friendRequest, new FriendRequests()));
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}
