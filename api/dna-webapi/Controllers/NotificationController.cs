﻿using DateNightDataServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightPersistence.Enums;
using DateNightApi.Services;
using DateNightApi.Models.DataBaseObjects;

namespace DateNightApi.Controllers
{
    [Route("api/Notification")]
    [ApiController]
    [Authorize]
    public class NotificationController : ControllerBase
    {
        private readonly ILogger<NotificationController> _logger;
        private UserService _userService;
        private FriendService _friendService;
        private FriendRequestService _friendRequestService;
        private ItemService _itemService;
        private VoteService _voteService;
        private VoteSessionService _voteSessionService;
        private NotificationService _notificationService;
        private GraphService _graphService;
        public NotificationController(ILogger<NotificationController> logger, UserService userService, FriendService friendService,
            FriendRequestService friendRequestService, ItemService itemService, VoteService voteService, VoteSessionService voteSessionService,
            NotificationService notificationService, GraphService graphService)
        {
            _logger = logger;
            _userService = userService;
            _friendRequestService = friendRequestService;
            _friendService = friendService;
            _itemService = itemService;
            _voteService = voteService;
            _voteSessionService = voteSessionService;
            _notificationService = notificationService;
            _graphService = graphService;
        }

        [Route("GetById")]
        [HttpGet]
        public async Task<ObjectResult> GetById(string id, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get Notification record using the id: {id}");
                Notification notification = _notificationService.GetById(id);
                Notifications notificationsMapped = Mapper.Map(notification, new Notifications());
                return Ok(notificationsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("Get")]
        [HttpGet]
        public async Task<ObjectResult> Get(string userId, string objectId, NotificationType? notificationType = null, NotificationStatus? notificationStatus = null)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get Notification records by userId: {userId}. notificationType {notificationType}, notificationStatus {notificationStatus}");
                var notifications = _notificationService.GetRecordsByUserId(userId, notificationType, notificationStatus);
                IEnumerable<Notifications> notificationsMapped = Mapper.Map(notifications, new List<Notifications>().AsEnumerable());
                return Ok(notificationsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("View")]
        [HttpGet]
        public async Task<ObjectResult> View(string userId, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get Notification records by userId: {userId}.");
                var notifications = _notificationService.ViewNotificationsByUserId(userId);
                IEnumerable<Notifications> notificationsMapped = Mapper.Map(notifications, new List<Notifications>().AsEnumerable());
                return Ok(notificationsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        [Route("InsertNotification")]
        [HttpPost]
        public async Task<ObjectResult> InsertNotification(Notification notification, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Inserting a Notification for user with id {notification.UserId}");

                _notificationService.InsertNotification(notification, ref isSuccess, ref message);

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(notification, new Notifications()));
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        [Route("UpdateNotification")]
        [HttpPost]
        public async Task<ObjectResult> UpdateNotification(Notification notification, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Updating a Notification with user id {notification.UserId}");


                _notificationService.UpdateNotification(notification, ref isSuccess, ref message);

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(notification, new Notifications()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }



        }

        [Route("UpdateNotificationStatus")]
        [HttpPost]
        public async Task<ObjectResult> UpdateNotificationStatus(string notificationId, NotificationStatus status, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Updating a Notification with notificationId {notificationId} to status {status}");


                _notificationService.UpdateNotificationStatus(notificationId, status, ref isSuccess, ref message);

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        var notification = _notificationService.GetById(notificationId);
                        return Ok(Mapper.Map(notification, new Notifications()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("DismissUserNotifications")]
        [HttpPost]
        public async Task<ObjectResult> DismissUserNotifications(string userKey, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }

                User userInQuestion = _userService.Get(userKey);
                _logger.LogInformation($"Clearing Notifications for user with key: {userKey}");


                _notificationService.DismissUserNotifications(userInQuestion.UserId.ToString(), ref isSuccess, ref message);

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok($"Cleared Notifications for user with key: {userKey}");
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}
