﻿using DateNightApi.Models.DataBaseObjects;
using DateNightDataServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{
    [Route("api/VoteSessionFilter")]
    [ApiController]
    [Authorize]
    public class VoteSessionFilterController : ControllerBase
    {
        private readonly ILogger<VoteSessionFilterController> _logger;
        private UserService _userService;
        private FilterService _filterService;
        private VoteSessionFilterService _voteSessionFilterService;
        private VoteSessionService _voteSessionService;
        private GraphService _graphService;

        public VoteSessionFilterController(ILogger<VoteSessionFilterController> logger,
            UserService userService, FilterService filterService, VoteSessionFilterService voteSessionFilterService,
            VoteSessionService voteSessionService, GraphService graphService)
        {
            _logger = logger;
            _userService = userService;
            _filterService = filterService;
            _voteSessionFilterService = voteSessionFilterService;
            _voteSessionService = voteSessionService;
            _graphService = graphService;
        }

        [Route("Get")]
        [HttpGet]
        public async Task<ObjectResult> Get(string key, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Getting votesessionfilter using key {key}");
                DateNightPersistence.Models.VoteSessionFilters vsfInDb = _voteSessionFilterService.Get(key);
                Models.DataBaseObjects.VoteSessionFiltersModel vsfMapped = Mapper.Map(vsfInDb, new Models.DataBaseObjects.VoteSessionFiltersModel());
                return Ok(vsfMapped);

            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetVoteSessionFilterRecordsByFilterId")]
        [HttpGet]
        public async Task<ObjectResult> GetVoteSessionFilterRecordsByFilterId(string filterId, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Getting votesessionfilter using filterId {filterId}");
                IEnumerable<DateNightPersistence.Models.VoteSessionFilters> vsfInDb = _voteSessionFilterService.GetVoteSessionFilterRecordsByFilterId(filterId);
                IEnumerable<Models.DataBaseObjects.VoteSessionFiltersModel> vsfMapped = Mapper.Map(vsfInDb, new List<Models.DataBaseObjects.VoteSessionFiltersModel>().AsEnumerable());
                return Ok(vsfMapped);

            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetVoteSessionFilterRecordsByVoteSessionId")]
        [HttpGet]
        public async Task<ObjectResult> GetVoteSessionFilterRecordsByVoteSessionId(string voteSessionId, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Getting votesessionfilter using voteSessionId {voteSessionId}");
                IEnumerable<DateNightPersistence.Models.VoteSessionFilters> vsfInDb = _voteSessionFilterService.GetVoteSessionFilterRecordsByVoteSessionId(voteSessionId);
                IEnumerable<Models.DataBaseObjects.VoteSessionFiltersModel> vsfMapped = Mapper.Map(vsfInDb, new List<Models.DataBaseObjects.VoteSessionFiltersModel>().AsEnumerable());
                return Ok(vsfMapped);

            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("InsertVoteSessionFilter")]
        [HttpPost]
        public async Task<ObjectResult> InsertVoteSessionFilter(DateNightPersistence.Models.VoteSessionFilters voteSessionFilter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                User user = _userService.Get(objectId);
                _logger.LogInformation($"{user.DisplayName} inserting a new vote session filter");
                #endregion

                _voteSessionFilterService.InsertVoteSessionFilter(voteSessionFilter, user, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(voteSessionFilter, new Models.DataBaseObjects.VoteSessionFiltersModel()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("CreateVoteSessionFilters")]
        [HttpPost]
        public async Task<ObjectResult> CreateVoteSessionFilters(List<DateNightPersistence.Models.Filter> filters, long voteSessionId, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Inserting new vote session filters for vote session id {voteSessionId}");
                #endregion

                var dbFilters = new List<Filter>();
                foreach (var filter in filters)
                {
                    var filterInDb = _filterService.GetByKeyWord(filter.KeyWord);

                    if (filterInDb == null)
                    {
                        filterInDb = _filterService.InsertFilter(filter, user: null, ref isSuccess, ref message);
                    }

                    dbFilters.Add(filterInDb);

                    _voteSessionFilterService.InsertVoteSessionFilter(new VoteSessionFilters
                    {
                        VoteSessionId = voteSessionId,
                        FilterId = filterInDb.Id,
                    }, user: null, ref isSuccess, ref message);
                }
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(dbFilters.AsEnumerable(), new List<Filters>().AsQueryable()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("UpdateVoteSessionFilter")]
        [HttpPost]
        public async Task<ObjectResult> UpdateVoteSessionFilter(DateNightPersistence.Models.VoteSessionFilters voteSessionFilter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                User user = _userService.Get(objectId);
                _logger.LogInformation($"{user.DisplayName} updating a vote session filter with id {voteSessionFilter.Id}");
                #endregion

                _voteSessionFilterService.UpdateVoteSessionFilter(voteSessionFilter, user, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(voteSessionFilter, new Models.DataBaseObjects.VoteSessionFiltersModel()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("RemoveVoteSessionFilter")]
        [HttpPost]
        public async Task<ObjectResult> RemoveVoteSessionFilter(DateNightPersistence.Models.VoteSessionFilters voteSessionFilter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"deleting vote session filter with id {voteSessionFilter.Id}");
                #endregion

                _voteSessionFilterService.RemoveVoteSessionFilter(voteSessionFilter, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok("Vote session filter deleted");
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}
