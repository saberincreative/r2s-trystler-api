﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightDataServices;
using System.Net.Http;
using DateNightPersistence.Enums;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{

    [Route("api/VoteSessions")]
    [ApiController]
    [Authorize]
    public class VoteSessionController : ControllerBase
    {
        private readonly ILogger<VoteSessionController> _logger;
        private UserService _userService;
        private VoteSessionService _voteSessionService; private GraphService _graphService;


        public VoteSessionController(ILogger<VoteSessionController> logger, UserService userService, VoteSessionService voteSessionService, GraphService graphService)
        {
            _logger = logger;
            _graphService = graphService;
            _userService = userService;
            _voteSessionService = voteSessionService;
        }

        /// <summary>
        /// The method sets the session status for a session object
        /// </summary>
        /// <param name="voteSessionObj">session object</param>
        /// <param name="sessionStatus">session status</param>
        /// <returns>call response</returns>
        [Route("SetVoteSessionStatus")]
        [HttpPost]
        public async Task<ObjectResult> SetVoteSessionStatus(VoteSession voteSessionObj, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                #region variable
                HttpResponseMessage response = new HttpResponseMessage();
                bool isSuccess = true;
                string message = "";

                // var user = _userService.Get("nabeel.ahmed@saberin.com");
                voteSessionObj.UserA = _userService.Get(voteSessionObj.AUserId.ToString());
                voteSessionObj.UserB = _userService.Get(voteSessionObj.BUserId.ToString());
                #endregion
                _logger.LogInformation($"Setting the status of a vote session with id {voteSessionObj.Id} between users ({voteSessionObj.UserA.DisplayName} & {voteSessionObj.UserB.DisplayName}) to {voteSessionObj.voteSessionStatus} ");
                var currentUser = _userService.Get(objectId);

                _voteSessionService.SetVoteSessionStatus(voteSessionObj, currentUser, ref isSuccess, ref message);

                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(voteSessionObj, new VoteSessions()));

                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// The method gets a list of vote sessions for a user that are active
        /// </summary>
        /// <param name="userId">user object id</param>
        /// <returns>list of vote session object</returns>
        [Route("GetUserActiveVoteSessionId")]
        [HttpGet]
        public async Task<ObjectResult> GetUserActiveVoteSessionId(string userId, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                //get user in question
                User userInQuestion = _userService.Get(userId);
                if (userInQuestion == null)
                {
                    return null;
                }
                _logger.LogInformation($"Getting all active vote sessions for user {userInQuestion.DisplayName}");
                //get list of vote sessions for user
                List<VoteSession> activeVoteSessions = userInQuestion.UserAVoteSession.ToList();
                activeVoteSessions.AddRange(userInQuestion.UserBVoteSession);

                //filter vote session where status is not complete
                IEnumerable<VoteSession> voteSessions = activeVoteSessions.AsQueryable().Where(s => !s.voteSessionStatus.Equals(VoteSessionStatus.Complete));
                IEnumerable<VoteSessions> voteSessionsMapped = Mapper.Map(voteSessions, new List<VoteSessions>().AsEnumerable());
                return Ok(voteSessionsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }


        }

        [Route("GetUserVoteSessions")]
        [HttpGet]
        public async Task<ObjectResult> GetUserVoteSessions(string userId,string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                //get user in question
                User userInQuestion = _userService.Get(userId);
                if (userInQuestion == null)
                {
                    return null;
                }
                _logger.LogInformation($"Getting all vote sessions for user {userInQuestion.DisplayName}");
                //get list of vote sessions for user
                List<VoteSession> VoteSessions = userInQuestion.UserAVoteSession.ToList();
                VoteSessions.AddRange(userInQuestion.UserBVoteSession);

                //filter out vote sessions where status is completed and scheduled time has passed
                IEnumerable<VoteSession> voteSessions = VoteSessions.AsQueryable()
                    .Where(s => !IsCompletedAndHasPassed(s));

                IEnumerable<VoteSessions> voteSessionsMapped = Mapper.Map(voteSessions, new List<VoteSessions>().AsEnumerable());
                return Ok(voteSessionsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }


        }

        [Route("GetUserVoteSessionsIncludingStatuses")]
        [HttpPost]
        public async Task<ObjectResult> GetUserVoteSessionsIncludingStatuses(string userId, List<VoteSessionStatus> sessionStatuses, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                //get user in question
                User userInQuestion = _userService.Get(userId);
                if (userInQuestion == null)
                {
                    return null;
                }
                _logger.LogInformation($"Getting all vote sessions for user {userInQuestion.DisplayName}");
                //get list of vote sessions for user
                List<VoteSession> VoteSessions = userInQuestion.UserAVoteSession.ToList();
                VoteSessions.AddRange(userInQuestion.UserBVoteSession);

                //filter out vote sessions where status is not in sessionStatuses, or is completed with a passed scheduled time
                IEnumerable<VoteSession> voteSessions = VoteSessions.AsQueryable();
                if (sessionStatuses != null && sessionStatuses.Count > 0)
                    voteSessions = voteSessions.Where(s => sessionStatuses.Contains(s.voteSessionStatus) && !IsCompletedAndHasPassed(s));
                IEnumerable<VoteSessions> voteSessionsMapped = Mapper.Map(voteSessions, new List<VoteSessions>().AsEnumerable());
                return Ok(voteSessionsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }


        }

        [Route("GetUserVoteSessionsExcludingStatuses")]
        [HttpPost]
        public async Task<ObjectResult> GetUserVoteSessionsExcludingStatuses(string userId, List<VoteSessionStatus> sessionStatuses, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                //get user in question
                User userInQuestion = _userService.Get(userId);
                if (userInQuestion == null)
                {
                    return null;
                }
                _logger.LogInformation($"Getting all vote sessions for user {userInQuestion.DisplayName}");
                //get list of vote sessions for user
                List<VoteSession> VoteSessions = userInQuestion.UserAVoteSession.ToList();
                VoteSessions.AddRange(userInQuestion.UserBVoteSession);

                //filter out vote session where status is in sessionStatuses, or is completed with a passed scheduled time
                IEnumerable<VoteSession> voteSessions = VoteSessions.AsQueryable();
                if (sessionStatuses != null && sessionStatuses.Count > 0)
                    voteSessions = voteSessions.Where(s => !sessionStatuses.Contains(s.voteSessionStatus) && !IsCompletedAndHasPassed(s));
                IEnumerable<VoteSessions> voteSessionsMapped = Mapper.Map(voteSessions, new List<VoteSessions>().AsEnumerable());
                return Ok(voteSessionsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }


        }

        [Route("GetVoteSessionById")]
        [HttpGet]
        public async Task<ObjectResult> GetVoteSessionById(string id, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get Vote session record using the id: {id}");
                VoteSession voteSession = _voteSessionService.Get(id);
                VoteSessions voteSessionMapped = Mapper.Map(voteSession, new VoteSessions());
                return Ok(voteSessionMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        private bool IsCompletedAndHasPassed(VoteSession session)
        {
            return session.voteSessionStatus == VoteSessionStatus.Complete && session.ScheduledDateTime < DateTime.UtcNow;
        }
    }
}