﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Microsoft.Identity.Web.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightPersistence.Enums;
using DateNightDataServices;
using System.Net.Http;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{
    [Route("api/Friend")]
    [ApiController]
    [Authorize]
    public class FriendController : ControllerBase
    {

        private readonly ILogger<FriendController> _logger;
        private FriendService _friendService;
        private UserService _userService;
        private GraphService _graphService;

        public FriendController(ILogger<FriendController> logger, FriendService friendService, UserService userService, GraphService graphService)
        {
            _logger = logger;
            _friendService = friendService;
            _userService = userService;
            _graphService = graphService;
        }




        [Route("Get")]
        [HttpGet]
        public async Task<ObjectResult> GetFriendRecord(string key, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get friend record using the key: {key}");
                Friend friend = _friendService.Get(key);
                Friends friendsMapped = Mapper.Map(friend, new Friends());
                return Ok(friendsMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetUserFriendRecords")]
        [HttpGet]
        public async Task<ObjectResult> GetUserFriendRecords(string userKey, string objectId, FriendStatus? status = null)
        {
            _logger.LogInformation($"Getting User Friend Records with userKey ({userKey}) and status ({status})");
            if (userKey != "" || userKey != null)
            {
                User userInQuestion = _userService.Get(userKey);
                try
                {
                    bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                    if (!accountEnabled)
                    {
                        throw new AccessViolationException("Account not enabled");
                    }
                    List<Friend> friendRecords = userInQuestion.UserAFriend.ToList();
                    friendRecords.AddRange(userInQuestion.UserBFriend);

                    if (status != null)
                    {
                        _logger.LogInformation($"Narrowing search to records with friend status {status}");
                        friendRecords = friendRecords.Where(x => x.FriendStatus.Equals(status)).ToList();
                    }
                    _logger.LogInformation($"With userKey ({userKey}) and status ({status}), obtained {friendRecords.Count} records");
                    IEnumerable<Friend> friendsInDb = friendRecords.AsQueryable();
                    IEnumerable<Friends> friendsMapped = Mapper.Map(friendsInDb, new List<Friends>().AsEnumerable());
                    return Ok(friendsMapped);
                }
                catch (AccessViolationException ex)
                {
                    _logger.LogError(ex, "access denied");
                    return StatusCode(403, ex.Message);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "failed to get");
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return null;
            }

        }


        [Route("SendFriendRequest")]
        [HttpPost]
        public async Task<ObjectResult> SendFriendRequest(Friend friend, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            _logger.LogInformation($"Creating friend request for userId {friend.UserId}, friend id {friend.UserFriendId}, identifier {friend.UserFriendIdentifier}, status {friend.FriendStatus}");
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                friend.User = _userService.Get(friend.UserId.ToString());
                friend.UserFriend = _userService.Get(friend.UserFriendId.ToString());

                _friendService.InsertFriend(friend, ref isSuccess, ref message);
                
                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(friend, new Friends()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("SetUserFriendIdByIdentifier")]
        [HttpPost]
        public async Task<ObjectResult> SetUserFriendIdByIdentifier(User user, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Using User object with id {user.UserId}, will attempt to obtain all friend records that have" +
                $" matching user friend identifier with status of invited and set the user friend id along with the status of request ");

                _friendService.UpdateFriendRecords(user, ref isSuccess, ref message);
                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        return Ok(message);
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("UpdateFriendRequest")]
        [HttpPost]
        public async Task<ObjectResult> UpdateFriendRequest(Friend friend, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Updating a friend request with id {friend.Id}");

                friend.User = _userService.Get(friend.UserId.ToString());
                friend.UserFriend = _userService.Get(friend.UserFriendId.ToString());
                User updater = _userService.Get(objectId);
                _friendService.UpdateFriend(friend, updater, ref isSuccess, ref message);

                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(friend, new Friends()));
                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}

