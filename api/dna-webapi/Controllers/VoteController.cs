﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Identity.Web.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightDataServices;
using System.Net.Http;
using DateNightPersistence.Enums;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{

    [Route("api/Vote")]
    [Authorize]

    [ApiController]
    public class VoteController : ControllerBase
    {
        private readonly ILogger<VoteController> _logger;
        private UserService _userService;
        private VoteService _voteService;
        private VoteSessionService _voteSessionService;
        private ItemService _itemService;
        private GraphService _graphService;

        public VoteController(ILogger<VoteController> logger, UserService userService, VoteService voteService, VoteSessionService voteSessionService, ItemService itemService, GraphService graphService)
        {
            _logger = logger;
            _userService = userService;
            _voteService = voteService;
            _voteSessionService = voteSessionService;
            _itemService = itemService;
            _graphService = graphService;
        }

        /// <summary>
        /// The method gets a queryable list of vote objects
        /// </summary>
        /// <returns>queryable list of votes</returns>
        [HttpGet]
        [Route("Get")]

        public async Task<ObjectResult> Get(string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get vote records");
                IEnumerable<Vote> votesFromDB = _voteService.GetQueryable().AsEnumerable();
                IEnumerable<Votes> votesMapped = Mapper.Map(votesFromDB, new List<Votes>().AsEnumerable());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        [HttpGet]
        [Route("GetVote")]
        public async Task<ObjectResult> GetVote(string voteId, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get vote record using id {voteId}");
                Vote voteInDb = _voteService.Get(voteId);

                Votes votesMapped = Mapper.Map(voteInDb, new Votes());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// The method tries to insert a collection of vote objects into the database
        /// </summary>
        /// <param name="userVotes">Collection of vote objects</param>
        /// <returns>call response</returns>
        [Route("InsertUserVotes")]
        [HttpPost]
        public async Task<ObjectResult> InsertUserVotes(ICollection<Vote> userVotes, string objectId, bool updateVoteSessionStatus = true)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                #region variables
                HttpResponseMessage response = new HttpResponseMessage();
                bool isSuccess = true;
                string message = "";
                _logger.LogInformation($"Inserting Votes");
                var user = _userService.Get(userVotes.First().UserId.ToString());
                #endregion
                _logger.LogInformation($"{user.DisplayName} inserting votes");
                // Loop through user votes
                foreach (Vote votes in userVotes)
                {
                    //Set User and VoteSession objects by getting them based on Id's associated with vote
                    votes.User = _userService.Get(votes.UserId.ToString());
                    votes.VoteSession = _voteSessionService.Get(votes.VoteSessionId.ToString());

                    //Try to get the item record associated with the item Id passed
                    //Item item = _itemService.Get(votes.Item.ItemId.ToString());

                    //if no item exist in db with the id
                    //if (item == null)
                    //{
                    //    _logger.LogInformation($"insert item record {votes.Item.ItemId}");
                    //    //insert item into db
                    //    _itemService.InsertNewItem(votes.Item, votes.User, ref isSuccess, ref message);
                    //}

                    //set item object by getting object based on Id associated with vote
                    //votes.Item = _itemService.Get(votes.Item.ItemId.ToString());

                    //set voted on flag to false for collection of uservotes
                    votes.VotedOn = false;

                }
                //try to insert user votes into db
                _logger.LogInformation($"{user.DisplayName} inserting votes into votesession with id {userVotes.First().VoteSessionId}");
                _voteService.InsertUserVotes(userVotes, user, ref isSuccess, ref message);


                switch (isSuccess)
                {
                    //if successful, update vote session to reflect new status
                    case true:
                        //response.StatusCode = System.Net.HttpStatusCode.OK;
                        if (updateVoteSessionStatus)
                        {
                            _logger.LogInformation($"Updating the vote session after successful insertion of votes for id {userVotes.First().VoteSessionId}");
                            UpdateVoteSession(userVotes.ToList()[0].VoteSession, userVotes.ToList()[0].User);
                        }
                        return Ok(Mapper.Map(userVotes, new List<Votes>().AsEnumerable()));


                    case false:
                        //response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        [Route("UpdateVoteStatus")]
        [HttpPost]
        public async Task<ObjectResult> UpdateVoteStatus(string voteId, string voteStatus, string objectId)
        {
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                User userInQuestion = _userService.Get(objectId);
                _logger.LogInformation($"Updating a vote with id {voteId} to status {voteStatus}, done by {userInQuestion.DisplayName}");
                _voteService.UpdateVoteStatus(voteId, voteStatus, userInQuestion, ref isSuccess, ref message);

                HttpResponseMessage response = new HttpResponseMessage();
                switch (isSuccess)
                {
                    case true:
                        Vote vote = _voteService.Get(voteId);
                        return Ok(Mapper.Map(vote, new Votes()));

                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        /// <summary>
        /// The method tries to update the vote session with a new status
        /// </summary>
        /// <param name="voteSession">session object to update</param>
        /// <param name="user">User object causing update</param>
        private void UpdateVoteSession(VoteSession voteSession, User user)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            _logger.LogInformation($"Update vote session for vote session id{voteSession.Id} done by user {user.DisplayName}");
            //determine if user in question is AUser or BUser
            bool isAUser = voteSession.AUserId.Equals(user.UserId) ? true : false;

            //get votes associated with session and user in question that have not been voted on
            IEnumerable<Vote> votes = _voteService.GetQueryable().Where(v => v.VoteSessionId.Equals(voteSession.Id) &&
            v.UserId.Equals(user.UserId) && !v.VotedOn);
            #endregion

            //switch based on vote count
            //if count is 1, then set the status to finalizing
            //if anything else (2+) then if user in question is AUser, set to BUserVotes else set to AUserVotes
            switch (votes.Count())
            {

                case 1:
                    _logger.LogInformation($"setting votesession status for vote session id {voteSession.Id} to {VoteSessionStatus.Finalizing}");
                    voteSession.voteSessionStatus = VoteSessionStatus.Finalizing;
                    _voteSessionService.SetVoteSessionStatus(voteSession, user, ref isSuccess, ref message);
                    break;
                default:
                    if (isAUser)
                    {
                        voteSession.voteSessionStatus = VoteSessionStatus.BUserVotes;
                        _logger.LogInformation($"setting votesession status for vote session id {voteSession.Id} to {VoteSessionStatus.BUserVotes}");
                        _voteSessionService.SetVoteSessionStatus(voteSession, user, ref isSuccess, ref message);
                    }
                    else
                    {
                        voteSession.voteSessionStatus = VoteSessionStatus.AUserVotes;
                        _logger.LogInformation($"setting votesession status for vote session id {voteSession.Id} to {VoteSessionStatus.AUserVotes}");
                        _voteSessionService.SetVoteSessionStatus(voteSession, user, ref isSuccess, ref message);
                    }
                    break;

            }

        }

        /// <summary>
        /// The method is called when someone says no to all choices
        /// The method set the voted on status for all votes associated with vote session to true
        /// In order to create a clean slate for another round of selecting
        /// </summary>
        /// <param name="session">session in question</param>
        /// <param name="user">user causing update</param>
        /// <returns>call response</returns>
        [Route("NoToAllInVoteSession")]
        [HttpPost]
        public async Task<ObjectResult> NoToAllInVoteSession(VoteSession session, string objectId)
        {
            #region variables
            HttpResponseMessage response = new HttpResponseMessage();
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                User user = _userService.Get(objectId);
                _logger.LogInformation($"Updating Votes for vote session id {session.Id} conducted by user {user.DisplayName}");
                #endregion

                _voteService.SetVotedOnForVotesInVoteSession(session, user, ref isSuccess, ref message);


                switch (isSuccess)
                {
                    case true:
                        return Ok(message);

                    case false:
                        return BadRequest(message);

                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        /// <summary>
        /// The method gets all vote objects associated with user
        /// </summary>
        /// <param name="userKey">user key used to get user object, can either be GUID or email</param>
        /// <returns>List of vote objects</returns>
        [Route("GetUserVotes")]
        [HttpGet]
        public async Task<ObjectResult> GetUserVotes(string userKey, string objectId)
        {
            User userInQuestion = _userService.Get(userKey);
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get user votes for user {userInQuestion.DisplayName}");
                IEnumerable<Vote> votesFromDB = _voteService.GetQueryable().Where(v => v.UserId.Equals(userInQuestion.UserId)).AsEnumerable();
                IEnumerable<Votes> votesMapped = Mapper.Map(votesFromDB, new List<Votes>().AsEnumerable());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }

        }

        /// <summary>
        /// The method gets the votes associated with a vote session and a user if specified
        /// </summary>
        /// <param name="sessionId">session object iodentifier</param>
        /// <param name="userKey">user key null by default</param>
        /// <returns>list of vote objects</returns>
        [Route("GetVoteSessionVotes")]
        [HttpGet]
        public async Task<ObjectResult> GetVoteSessionVotes(string sessionId, string objectId, string userKey = null)
        {
            User userInQuestion = new User();
            _logger.LogInformation($"Getting Vote session votes for id {sessionId}");
            //if user key is not null get user object
            if (userKey != null)
            {

                userInQuestion = _userService.Get(userKey);
                _logger.LogInformation($"Getting Vote session votes for user {userInQuestion.DisplayName}");
            }
            else
            {
                userInQuestion = null;
            }

            //get vote session object
            VoteSession session = _voteSessionService.Get(sessionId);
            try
            {

                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }

                //get votes associated with session
                IEnumerable<Vote> votes = _voteService.GetQueryable().Where(v => v.VoteSessionId.Equals(session.Id));

                //if user object is not null
                if (userInQuestion != null)
                {
                    //filter votes list to get only votes associated with user
                    votes = votes.Where(v => v.UserId.Equals(userInQuestion.UserId));
                }

                IEnumerable<Votes> votesMapped = Mapper.Map(votes, new List<Votes>().AsEnumerable());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// The method get a list of vote objects associated with the other user in the session that are affirmative and not been voted on
        /// </summary>
        /// <param name="sessionId">session object id</param>
        /// <param name="userKey">user object key</param>
        /// <returns></returns>
        [Route("GetOtherSessionUserNonVotedOnVotes")]
        [HttpGet]
        public async Task<ObjectResult> GetOtherSessionUserNonVotedOnVotes(string sessionId, string userKey, string objectId)
        {
            try
            {

                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                //set user and session object
                User userInQuestion = _userService.Get(userKey);
                VoteSession session = _voteSessionService.Get(sessionId);

                //get the other user id
                Guid otherUserId = session.AUserId.Equals(userInQuestion.UserId) ? session.BUserId : session.AUserId;

                //get votes associated with vote session and other user that are affirmative and not been voted on
                IEnumerable<Vote> votes = _voteService.GetQueryable().Where(v => v.VoteSessionId.Equals(session.Id) &&
                v.UserId.Equals(otherUserId) && !v.VotedOn);
                _logger.LogInformation($"Getting unvoted votes associated to session {session.Id} and user {otherUserId}");
                IEnumerable<Votes> votesMapped = Mapper.Map(votes, new List<Votes>().AsEnumerable());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetVoteSessionAffirmativeVotes")]
        [HttpGet]
        public async Task<ObjectResult> GetVoteSessionAffirmativeVotes(string sessionId, string objectId)
        {
            try
            {

                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                VoteSession session = _voteSessionService.Get(sessionId);
                IEnumerable<Vote> votes = _voteService.GetQueryable().Where(v => v.VoteSessionId.Equals(session.Id) && v.IsAffirmative);
                _logger.LogInformation($"get votes marked as isAffirmative for session {session.Id}");
                IEnumerable<Votes> votesMapped = Mapper.Map(votes, new List<Votes>().AsEnumerable());
                return Ok(votesMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}