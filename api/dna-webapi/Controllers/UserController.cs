﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateNightPersistence.Models;
using DateNightDataServices;
using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;

namespace DateNightApi.Controllers
{

    [Route("api/User")]
    [ApiController]
    [Authorize]

    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private UserService _userService;
        private GraphService _graphService;

        public UserController(ILogger<UserController> logger, UserService userService, GraphService graphService)
        {
            _logger = logger;
            _userService = userService;
            _graphService = graphService;
        }

        /// <summary>
        /// The method gets a user object using the key provided
        /// </summary>
        /// <param name="userKey">user key used to obtain user Object, can be either the email or GUID</param>
        /// <returns>User object</returns>
        [Route("GetUser")]
        [HttpGet]
        public async Task<ObjectResult> GetUser(string userKey, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get user record using the key: {userKey}");
                User users = _userService.Get(userKey);
                Users usersMapped = Mapper.Map(users, new Users());
                return Ok(usersMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// The method tries to insert a User object into the database
        /// </summary>
        /// <param name="user">User object</param>
        /// <returns>call response</returns>
        [Route("InsertUser")]
        [HttpPost]
        public async Task<ObjectResult> InsertUser(User user, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Inserting a user: ID {user.UserId}, First Name {user.FirstName}, Last Name {user.LastName}, Email {user.Email}, Display Name {user.DisplayName}");
                #endregion

                _userService.InsertUser(user, ref isSuccess, ref message);

                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(user, new Users()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        /// <summary>
        /// The method tries to update an existing user object
        /// </summary>
        /// <param name="user">user to update</param>
        /// <returns>call response</returns>
        [Route("UpdateUser")]
        [HttpPost]
        public async Task<ObjectResult> UpdateUser(User user, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Updating a user with user id {user.UserId}");
                #endregion

                _userService.UpdateUser(user, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(user, new Users()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("DeleteUser")]
        [HttpPost]
        public async Task<ObjectResult> DeleteUser(User user, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Deleting a user with user id {user.UserId}");
                #endregion

                isSuccess = await _graphService.DeleteUserAsync(user.UserId.ToString());

                if (isSuccess)
                {
                    isSuccess = _userService.DeleteUser(user, ref isSuccess, ref message);
                }
                
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(user, new Users()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetOtherUsers")]
        [HttpGet]
        public async Task<ObjectResult> GetOtherUsers(Guid userKey, string objectId)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Get other user record using the key: {userKey}");
                IEnumerable<User> usersInDb = _userService.GetQueryable().Where(u => (!u.UserId.Equals(userKey)));
                IEnumerable<Users> usersMapped = Mapper.Map(usersInDb, new List<Users>().AsEnumerable());
                return Ok(usersMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}
