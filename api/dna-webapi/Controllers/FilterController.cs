﻿using DateNightApi.Models.DataBaseObjects;
using DateNightApi.Services;
using DateNightDataServices;
using DateNightPersistence.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Controllers
{
    [Route("api/Filter")]
    [ApiController]
    [Authorize]
    public class FilterController : ControllerBase
    {
        private readonly ILogger<FilterController> _logger;
        private UserService _userService;
        private FilterService _filterService; 
        private GraphService _graphService;
        public FilterController(ILogger<FilterController> logger,
          UserService userService, FilterService filterService,
          GraphService graphService)
        {
            _logger = logger;
            _userService = userService;
            _filterService = filterService;
            _graphService = graphService;

        }

        [Route("Get")]
        [HttpGet]
        public async Task<ObjectResult> Get(string key, string objectId, bool getDefault = false, string keyWord = null, string voteType = null, string filterType = null)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Getting filter using the following values: key {key}, default object {getDefault}, keyWord {keyWord}, voteType {voteType}, filterType {filterType}");
                Filter filterInDb = _filterService.Get(key, getDefault, keyWord, voteType, filterType);
                Filters filterMapped = Mapper.Map(filterInDb, new Filters());
                return Ok(filterMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("GetFilters")]
        [HttpGet]
        public async Task<ObjectResult> GetFilters(string key, string objectId, bool getDefault = false, string keyWord = null, string voteType = null, string filterType = null)
        {
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Getting filters using the following values: key {key}, default object {getDefault}, keyWord {keyWord}, voteType {voteType}, filterType {filterType}");
                var filters = _filterService.GetFilters(key, getDefault, keyWord, voteType, filterType);
                IEnumerable<Filters> filtersMapped = Mapper.Map(filters, new List<Filters>().AsEnumerable());
                return Ok(filtersMapped);
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("InsertFilter")]
        [HttpPost]
        public async Task<ObjectResult> InsertFilter(Filter filter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            User user = _userService.Get(objectId);
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"{user.DisplayName} inserting a new filter");
                #endregion

                _filterService.InsertFilter(filter, user, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(filter, new Filters()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("UpdateFilter")]
        [HttpPost]
        public async Task<ObjectResult> UpdateFilter(Filter filter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            User user = _userService.Get(objectId);
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"{user.DisplayName} updating filter with id {filter.Id}");
                #endregion

                _filterService.UpdateFilter(filter, user, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok(Mapper.Map(filter, new Filters()));
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }

        [Route("RemoveFilter")]
        [HttpPost]
        public async Task<ObjectResult> RemoveFilter(Filter filter, string objectId)
        {
            #region variables
            bool isSuccess = true;
            string message = "";
            User user = _userService.Get(objectId);
            try
            {
                bool accountEnabled = await _graphService.IsAccountEnabled(objectId);
                if (!accountEnabled)
                {
                    throw new AccessViolationException("Account not enabled");
                }
                _logger.LogInformation($"Deleting filter record with id {filter.Id}");
                #endregion

                _filterService.RemoveFilter(filter, ref isSuccess, ref message);
                switch (isSuccess)
                {
                    case true:
                        return Ok("Filter and associated Vote Session Filters deleted");
                    case false:
                        return BadRequest(message);
                }
            }
            catch (AccessViolationException ex)
            {
                _logger.LogError(ex, "access denied");
                return StatusCode(403, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "failed to get");
                return StatusCode(403, ex.Message);
            }
        }
    }
}
