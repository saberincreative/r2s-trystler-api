﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Options
{
    public class GraphApiConfig
    {
        public string ClientId { get; set; }
        public string TenantId { get; set; }
        public string ClientSecret { get; set; }

        public string ExtensionAppId { get; set; }
    }
}
