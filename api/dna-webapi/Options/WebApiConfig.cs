﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateNightApi.Options
{
    public class WebApiConfig
    {
        public string ApiUsername { get; set; }

        public string ApiPassword { get; set; }
    }
}
