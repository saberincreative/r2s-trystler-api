﻿using DateNightApi.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DateNightApi.Authentication
{
    public class BasicAuthFilter : IAuthorizationFilter
    {
        private readonly IOptions<WebApiConfig> _config;

        private readonly ILogger<BasicAuthFilter> _logger;
        public BasicAuthFilter(IOptions<WebApiConfig> config, ILogger<BasicAuthFilter> logger)
        {
            _config = config;
            _logger = logger;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                string authHeader = context.HttpContext.Request.Headers["Authorization"];
                
                if (authHeader != null)
                {
                    var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
                    if (authHeaderValue.Scheme.Equals(AuthenticationSchemes.Basic.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        var credentials = Encoding.UTF8
                            .GetString(Convert.FromBase64String(authHeaderValue.Parameter ?? string.Empty))
                            .Split(':', 2);
                        if (credentials.Length == 2)
                        {
                            if (IsAuthorized(context, credentials[0], credentials[1]))
                            {
                
                                return;
                            }
                        }
                    }
                }

                ReturnUnauthorizedResult(context);
            }
            catch (FormatException e)
            {
                _logger.LogError("Unauthorized", e);
                ReturnUnauthorizedResult(context);
            }
        }

        public bool IsAuthorized(AuthorizationFilterContext context, string username, string password)
        {
            //var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
            return IsValidUser(username, password);
        }

        private bool IsValidUser(string username, string password)
        {
           
            if (username == _config.Value.ApiUsername && password == _config.Value.ApiPassword)
            {
                return true;
            }

            return false;
        }

        private void ReturnUnauthorizedResult(AuthorizationFilterContext context)
        {
            // Return 401 and a basic authentication challenge (causes browser to show login dialog)
            context.HttpContext.Response.Headers["WWW-Authenticate"] = "Basic realm=\"REALM\"";
            context.Result = new UnauthorizedResult();
        }
    }
}
