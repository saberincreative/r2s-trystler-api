﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum VoteStatus
    {
        None = 0,
        Cancelled = 100,
        Submitted = 200,
        Completed = 300
    }
}
