﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum NotificationType
    {
        FriendRequestAccepted = 0,
        FriendRequestDeclined = 1,
        FriendRequestBlocked = 2,
        VoteSessionCreated = 3,
        VoteSessionYourTurn = 4,
        VoteSessionCancelled = 5,
        VoteSessionFinalized = 6,
        VoteSessionCompleted = 7,
        VoteSessionExpired = 8,
        VoteSessionDateTimeUpdated = 9,
    }
}
