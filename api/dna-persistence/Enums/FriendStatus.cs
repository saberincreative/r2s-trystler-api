﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
   public enum FriendStatus
    {
        Invited = 0,
        Requested = 100,
        Accepted = 200,
        Declined = 300,
        Cancelled = 400,
        Unfriended = 500,
        Blocked = 600
    }
}
