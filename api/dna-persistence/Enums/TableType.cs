﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum TableType
    {
        Friend = 0,
        FriendRequest = 1,
        VoteSession = 2,
        User = 3,
        Vote = 4,
        Item = 5,
    }
}
