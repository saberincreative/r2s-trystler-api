﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum NotificationStatus
    {
        NotViewed = 0,
        Viewed = 1,
        Dismissed = 2,
    }
}
