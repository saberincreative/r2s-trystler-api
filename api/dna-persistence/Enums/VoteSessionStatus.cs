﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum VoteSessionStatus : int
    {
        NotStarted = 0,
        Complete = 100,
        AUserVotes = 200,
        BUserVotes = 300,
        Finalizing = 400,
        Expired = 500,
        Cancelled = 600,
    }
}
