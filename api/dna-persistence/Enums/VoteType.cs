﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Enums
{
    public enum VoteType : int
    {
        Place = 0,
        Restaurant = 100,
        Movie = 200,
        RestaurantAndMovie = 300,
    }
}
