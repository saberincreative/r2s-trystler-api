﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DateNightPersistence.Models
{
    public class User
    {
        [Key]
        public Guid UserId { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public DateTime? UpdatedDateUtc { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? CreatedDateUtc { get; set; }
        public string CreatedBy { get; set; }

        public bool? Active { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string DeviceToken { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }

        public virtual ICollection<VoteSession> UserAVoteSession { get; set; }
        public virtual ICollection<VoteSession> UserBVoteSession { get; set; }

        public virtual ICollection<Friend> UserAFriend { get; set; }
        public virtual ICollection<Friend> UserBFriend { get; set; }

        public virtual ICollection<FriendRequest> Sender { get; set; }
        public virtual ICollection<FriendRequest> Recipient { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }

        public virtual ICollection<Filter> Filters { get; set; }
    }
}