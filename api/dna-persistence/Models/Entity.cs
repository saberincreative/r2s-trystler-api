﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public partial class Entity
    {
        public long Id { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? UpdatedDateUtc { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? CreatedDateUtc { get; set; }
        public string CreatedBy { get; set; }

    }
}
