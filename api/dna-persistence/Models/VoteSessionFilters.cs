﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class VoteSessionFilters : Entity
    {
        [ForeignKey("VoteSessionFilters")]
        public long VoteSessionId{ get; set; }

        [ForeignKey("VoteSessionFilters")]
        public long FilterId { get; set; }

        public virtual Filter Filter { get; set; }

        public virtual VoteSession VoteSession { get; set; }
    }
}
