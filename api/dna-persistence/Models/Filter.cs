﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class Filter : Entity
    {
        public VoteType VoteType { get; set; }

        public string KeyWord { get; set; }

        [ForeignKey("Filters")]
        public Guid? UserId { get; set; }

        public virtual User User { get; set; }

        public bool IsDefault { get; set; }

        public FilterType FilterType { get; set; }

        public virtual ICollection<VoteSessionFilters> VoteSessionFilters { get; set; }
    }
}


namespace DateNightPersistence
{
    using DateNightPersistence.Models;

    public partial class Configurator
    {
        public void Filters()
        {
            modelBuilder.Entity<Filter>().HasData(
                new Filter
                {
                    Id = 1,
                    KeyWord = "Fast Food",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 2,
                    KeyWord = "Vegetarian",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 3,
                    KeyWord = "Café",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 4,
                    KeyWord = "Coffee and Tea",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 5,
                    KeyWord = "Snacks",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 6,
                    KeyWord = "Breakfast",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.MealOfDay,
                },
                new Filter
                {
                    Id = 7,
                    KeyWord = "Lunch",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.MealOfDay,
                },
                new Filter
                {
                    Id = 8,
                    KeyWord = "Dinner",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.MealOfDay,
                },
                new Filter
                {
                    Id = 9,
                    KeyWord = "Bakery",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 10,
                    KeyWord = "Salad",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 11,
                    KeyWord = "Dessert",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 12,
                    KeyWord = "Pizza",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 13,
                    KeyWord = "American",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 14,
                    KeyWord = "Mexican",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 15,
                    KeyWord = "Italian",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                },
                new Filter
                {
                    Id = 16,
                    KeyWord = "Asian",
                    IsDefault = true,
                    VoteType = VoteType.Restaurant,
                    FilterType = FilterType.Cuisine,
                }
            );
        }
    }
}