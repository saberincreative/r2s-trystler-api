﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class Notification : Entity
    {
        [ForeignKey("Notifications")]
        public Guid UserId { get; set; }
        public string Message { get; set; }
        public NotificationType NotificationType { get; set; }
        public NotificationStatus NotificationStatus { get; set; }
        public TableType TableType { get; set; }
        public string RecordId { get; set; }

        public virtual User User { get; set; }
    }
}
