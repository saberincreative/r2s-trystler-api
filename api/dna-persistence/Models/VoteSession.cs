﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class VoteSession : Entity
    {
        public string Title { get; set; }

        [ForeignKey("UserAVoteSession")]
        public Guid AUserId { get; set; }

        [ForeignKey("UserBVoteSession")]
        public Guid BUserId { get; set; }

        public virtual User UserA { get; set; }

        public virtual User UserB { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }

        public VoteSessionStatus voteSessionStatus { get; set; }

        public int OptionReduction { get; set; }

        public string SearchTerms { get; set; }

        public string OriginalSearchTerms { get; set; }

        public string FinalizedItemIds { get; set; }

        public string Location { get; set; }

        public DateTime ScheduledDateTime { get; set; }

        public bool isDateTimeStrict { get; set; }

        public virtual ICollection<VoteSessionFilters> VoteSessionFilters { get; set; }
    }
}
