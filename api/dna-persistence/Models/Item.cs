﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class Item : Entity
    {
        public string Text { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        //public long Votes { get; set; }

        public string ItemId { get; set;  }
        public VoteType VoteType { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
