﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class FriendRequest : Entity
    {
        [ForeignKey("Sender")]
        public Guid SenderId { get; set; }
        public virtual User Sender { get; set; }
        [ForeignKey("Recipient")]
        public Guid? RecipientId { get; set; }
        public virtual User Recipient { get; set; }
        public Guid ShareKey { get; set; }
    }
}
