﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public partial class ApplicationLog
{
    public int Id { get; set; }
    public string Level { get; set; }
    public DateTime? TimeStamp { get; set; }
    public string Exception { get; set; }
    public string Message { get; set; }
    public string UserName { get; set; }
    public string Controller { get; set; }
    public string Project { get; set; }
    public string IPAddress { get; set; }
    public string SessionId { get; set; }
    public string Method { get; set; }
    public string Hostname { get; set; }
}
}
