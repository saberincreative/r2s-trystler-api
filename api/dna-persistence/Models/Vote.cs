﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class Vote : Entity
    {
        public string ItemId { get; set; }
        public VoteType VoteType { get; set; }
        [ForeignKey("Votes")]
        public Guid UserId { get; set; }

        [ForeignKey("Votes")]
        public long VoteSessionId { get; set; }

        public bool IsAffirmative { get; set; }
        public bool IsNoVote { get; set; }

        public VoteStatus VoteStatus { get; set; }

        public virtual User User { get; set; }

        //public VoteType VoteType { get; set; }
        public virtual VoteSession VoteSession { get; set; }

        public bool VotedOn { get; set; }
    }
}
