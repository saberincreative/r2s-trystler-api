﻿using DateNightPersistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightPersistence.Models
{
    public class Friend : Entity
    {
        public virtual User User { get; set; }

        [ForeignKey("UserAFriend")]
        public Guid UserId { get; set; }

        public virtual User UserFriend { get; set; }

        public string UserFriendIdentifier { get; set; }

        [ForeignKey("UserBFriend")]
        public Guid? UserFriendId { get; set; }

        public FriendStatus FriendStatus { get; set; }

        public string? FriendCode { get; set; }
    }
}
