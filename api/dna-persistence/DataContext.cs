﻿using DateNightPersistence.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DateNightPersistence
{
    public partial class DataContext : DbContext
    {
        public DataContext() { }

        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        private string _connectionString { get; set; }
        public DataContext(DbContextOptions<DataContext> options)
          : base(options)
        {
        }

        //public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }

        public virtual DbSet<ApplicationLog> ApplicationLog { get; set; }

        public virtual DbSet<VoteSession> VoteSession { get; set; }

        public virtual DbSet<Friend> Friend { get; set; }
        public virtual DbSet<FriendRequest> FriendRequest { get; set; }

        public virtual DbSet<Notification> Notification { get; set; }

        public virtual DbSet<Filter> Filter { get;  set; }

        public virtual DbSet<VoteSessionFilters> VoteSessionFilter { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var configure = new Configurator(modelBuilder);

            configure.Filters();

            modelBuilder.Entity<ApplicationLog>(entity =>
            {
                entity.ToTable("ApplicationLog");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Level)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Exception)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Message)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Project)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IPAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SessionId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Method)
                    .HasMaxLength(255)
                    .IsUnicode(false);


            });

            //modelBuilder.Entity<Item>(entity =>
            //{
            //    entity.HasKey(e => e.Id);

            //    entity.Property(e => e.ItemId)
            //        .IsRequired();
                    


            //});

            modelBuilder.Entity<User>(entity =>
            {

                entity.Property(e => e.UserId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DisplayName)
                   .IsRequired()
                   .HasMaxLength(255)
                   .IsUnicode(false)
                   .HasDefaultValueSql("('=firstname + '' '' + lastname')");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
              .IsRequired()
              .HasMaxLength(256)
              .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Vote>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.IsAffirmative)
                  .IsRequired();

                entity.Property(e => e.VotedOn)
                  .IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Votes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_Vote_User");

                entity.Property(e => e.ItemId)
                .IsRequired();

                entity.HasOne(d => d.VoteSession)
                    .WithMany(p => p.Votes)
                    .HasForeignKey(d => d.VoteSessionId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_Vote_VoteSession");

            });

            modelBuilder.Entity<VoteSession>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.voteSessionStatus)
                  .IsRequired();

                entity.HasOne(d => d.UserA)
                    .WithMany(p => p.UserAVoteSession)
                    .HasForeignKey(d => d.AUserId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_VoteSession_UserA");

                entity.HasOne(d => d.UserB)
                    .WithMany(p => p.UserBVoteSession)
                    .HasForeignKey(d => d.BUserId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_VoteSession_UserB");
                entity.Property(e => e.ScheduledDateTime).IsRequired();
                entity.Property(e => e.isDateTimeStrict).IsRequired().HasDefaultValue(false);
            });
            modelBuilder.Entity<Friend>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.FriendStatus)
                  .IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAFriend)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_Friend_UserA");

                entity.HasOne(d => d.UserFriend)
                    .WithMany(p => p.UserBFriend)
                    .HasForeignKey(d => d.UserFriendId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_Friend_UserB");


            });

            modelBuilder.Entity<FriendRequest>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.ShareKey)
                  .IsRequired();

                entity.HasOne(d => d.Sender)
                    .WithMany(p => p.Sender)
                    .HasForeignKey(d => d.SenderId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_FriendRequest_Sender");

                entity.HasOne(d => d.Recipient)
                    .WithMany(p => p.Recipient)
                    .HasForeignKey(d => d.RecipientId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_FriendRequest_Recipient");


            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Message)
                  .IsRequired();

                entity.Property(e => e.UserId)
                  .IsRequired();

                entity.Property(e => e.NotificationType)
                 .IsRequired();

                entity.Property(e => e.NotificationStatus)
                 .IsRequired();

                entity.Property(e => e.TableType)
                 .IsRequired();

                entity.Property(e => e.RecordId)
                 .IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientCascade)
                    .HasConstraintName("FK_Notification_User");
            });

            modelBuilder.Entity<Filter>(entity =>
            {
                entity.ToTable("Filter");

                entity.HasKey(e => e.Id);

                entity.HasOne(d => d.User)
                 .WithMany(p => p.Filters)
                 .HasForeignKey(d => d.UserId)
                 .OnDelete(DeleteBehavior.ClientCascade)
                 .HasConstraintName("FK_Filter_User");

                entity.Property(e => e.IsDefault)
                .IsRequired().HasDefaultValue(false); 
            });

            modelBuilder.Entity<VoteSessionFilters>(entity =>
            {
                entity.ToTable("VoteSessionFilter");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.FilterId).IsRequired();
                entity.Property(e => e.VoteSessionId).IsRequired();

                entity.HasOne(d => d.Filter)
                .WithMany(p => p.VoteSessionFilters)
                .HasForeignKey(d => d.FilterId)
                .OnDelete(DeleteBehavior.ClientCascade)
                .HasConstraintName("FK_VSF_Filter");

                entity.HasOne(d => d.VoteSession)
                .WithMany(p => p.VoteSessionFilters)
                .HasForeignKey(d => d.VoteSessionId)
                .OnDelete(DeleteBehavior.ClientCascade)
                .HasConstraintName("FK_VSF_VS");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
