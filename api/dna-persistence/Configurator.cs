﻿using Microsoft.EntityFrameworkCore;

namespace DateNightPersistence
{
    public partial class Configurator
    {
        private ModelBuilder modelBuilder { get; set; }
        public Configurator(ModelBuilder _modelBuilder)
        {
            modelBuilder = _modelBuilder;
        }
    }
}
