﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dnapersistence.Migrations
{
    public partial class TRY466 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Filter",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "CreatedDateUtc", "IsDefault", "KeyWord", "UpdatedBy", "UpdatedDate", "UpdatedDateUtc", "UserId", "VoteType" },
                values: new object[,]
                {
                    { 1L, null, null, null, true, "Fast Food", null, null, null, null, 100 },
                    { 2L, null, null, null, true, "Vegetarian", null, null, null, null, 100 },
                    { 3L, null, null, null, true, "Café", null, null, null, null, 100 },
                    { 4L, null, null, null, true, "Coffee and Tea", null, null, null, null, 100 },
                    { 5L, null, null, null, true, "Snacks", null, null, null, null, 100 },
                    { 6L, null, null, null, true, "Breakfast", null, null, null, null, 100 },
                    { 7L, null, null, null, true, "Lunch", null, null, null, null, 100 },
                    { 8L, null, null, null, true, "Dinner", null, null, null, null, 100 },
                    { 9L, null, null, null, true, "Bakery", null, null, null, null, 100 },
                    { 10L, null, null, null, true, "Salad", null, null, null, null, 100 },
                    { 11L, null, null, null, true, "Dessert", null, null, null, null, 100 },
                    { 12L, null, null, null, true, "Pizza", null, null, null, null, 100 },
                    { 13L, null, null, null, true, "American", null, null, null, null, 100 },
                    { 14L, null, null, null, true, "Mexican", null, null, null, null, 100 },
                    { 15L, null, null, null, true, "Italian", null, null, null, null, 100 },
                    { 16L, null, null, null, true, "Asian", null, null, null, null, 100 }
                });

            migrationBuilder.AddColumn<string>(
                name: "DeviceToken",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DropColumn(
                name: "DeviceToken",
                table: "Users");
        }
    }
}
