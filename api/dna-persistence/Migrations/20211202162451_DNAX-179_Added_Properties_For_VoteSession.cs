﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dnapersistence.Migrations
{
    public partial class DNAX179_Added_Properties_For_VoteSession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FinalizedItemIds",
                table: "VoteSession",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OriginalSearchTerms",
                table: "VoteSession",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SearchTerms",
                table: "VoteSession",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinalizedItemIds",
                table: "VoteSession");

            migrationBuilder.DropColumn(
                name: "OriginalSearchTerms",
                table: "VoteSession");

            migrationBuilder.DropColumn(
                name: "SearchTerms",
                table: "VoteSession");
        }
    }
}
