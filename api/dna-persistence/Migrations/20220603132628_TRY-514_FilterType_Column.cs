﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dnapersistence.Migrations
{
    public partial class TRY514_FilterType_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FilterType",
                table: "Filter",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 1L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 2L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 3L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 4L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 5L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 9L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 10L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 11L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 12L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 13L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 14L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 15L,
                column: "FilterType",
                value: 100);

            migrationBuilder.UpdateData(
                table: "Filter",
                keyColumn: "Id",
                keyValue: 16L,
                column: "FilterType",
                value: 100);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilterType",
                table: "Filter");
        }
    }
}
