trigger: none

variables:
- name: BuildPlatform
  value: "Any CPU"
- ${{ if startsWith(variables['Build.SourceBranch'], 'refs/heads/release/') }}:
  - name: BuildConfiguration
    value: $[ replace(variables['Build.SourceBranch'], 'refs/heads/release/', '') ]
- ${{ else }}:
  - name: BuildConfiguration
    value: Debug


stages:
- stage: Build
  jobs:
    - job: Init
      pool:
        name: self-hosted
      steps:
      - checkout: none
      - powershell: |
          [String]$buildNumber=$Env:BUILD_BUILDNUMBER
          [String]$buildYear=$buildNumber.SubString(0,4)
          [String]$buildMonth=$buildNumber.SubString(4,2)
          [String]$buildDay=$buildNumber.SubString(6,2)
          [String]$buildRev=$buildNumber.Split('.')[1]
          [String]$versionNumber="2.${buildRev}.${buildYear}.${buildMonth}${buildDay}"
          [String]$versionString="Version(""${versionNumber}"")"
          write-host versionNumber $versionNumber
          write-host versionString $versionString
          Write-Host "##vso[task.setvariable variable=VersionNumber;isOutput=true;]$versionNumber"
          Write-Host "##vso[task.setvariable variable=VersionString;isOutput=true;]$versionString"
        
        name: "SetVersion"
    - job: BuildDotNet
      dependsOn: Init

      pool:
        name: self-hosted
        demands:
        - msbuild
      variables:
      - name: VersionString
        value: $[ dependencies.Init.outputs['SetVersion.VersionString'] ]
      - name: VersionNumber
        value: $[ dependencies.Init.outputs['SetVersion.VersionNumber'] ]

      steps:
      - powershell: |
          [String]$VersionString=$Env:VERSIONSTRING
          write-host VersionString $VersionString

        name: "DisplayVersion"

      - checkout: self

      - powershell: |
          [String]$VersionString=$Env:VERSIONSTRING
          write-host VersionString $VersionString
          write-host "set version in assembly.cs files"
          $files = Get-ChildItem -Include "AssemblyInfo.cs" -Recurse
          foreach ($file in $files)
          {
            write-host $file
            (Get-Content $file) -replace 'Version[(]\".*[.].*[.].*\"[)]', $VersionString | Out-File $file
          }          

        name: "ApplyVersion"

      - task: DotNetCoreCLI@2
        inputs:
          command: restore
          projects: 'api/dna-webapi.sln'
          feedsToUse: config
          nugetConfigPath: nuget.config
        name: nuget

      - task: DotNetCoreCLI@2
        displayName: "build_webapi"
        inputs:
          modifyOutputPath: false
          command: publish
          publishWebProjects: False
          projects: 'api/dna-webapi.sln'
          arguments: '--no-restore -c $(buildConfiguration) /p:EnvironmentName=$(buildConfiguration) /p:AssemblyVersion=$(VersionNumber) /p:SkipInvalidConfigurations=false -p:Version=$(VersionNumber) -o "$(build.artifactStagingDirectory)/webapi"'

      - task: PublishPipelineArtifact@1
        inputs:
          targetPath: '$(Build.ArtifactStagingDirectory)'
          publishLocation: 'pipeline'

- stage: DeployTrystlerApiProduction
  dependsOn:
    Build
  condition:
    and(succeeded(),eq(variables['Build.SourceBranch'], 'refs/heads/release/production'))
  jobs:
    - deployment: DeployTrystlerApi
      environment:
        resourceType: VirtualMachine
        name: production
        tags: api
      strategy:
        runOnce:
          preDeploy:
            steps:
            - task: DownloadBuildArtifacts@1
              inputs:
                buildType: 'current'
                downloadType: 'single'
                artifactName: 'Build.BuildDotnet'
                itemPattern: '**\*.zip'
                downloadPath: '$(System.ArtifactsDirectory)'
          deploy:
            steps:
            - task: IISWebAppManagementOnMachineGroup@0
              inputs:
                EnableIIS: true
                IISDeploymentType: 'IISWebsite'
                ActionIISWebsite: 'CreateOrUpdateWebsite'
                WebsiteName: 'trystlerApi'
                WebsitePhysicalPath: '%SystemDrive%\inetpub\trystlerApi'
                WebsitePhysicalPathAuth: 'WebsiteUserPassThrough'
                CreateOrUpdateAppPoolForWebsite: true
                AppPoolNameForWebsite: 'trystlerApi'
                DotNetVersionForWebsite: 'v4.0'
                PipeLineModeForWebsite: 'Integrated'
                AppPoolIdentityForWebsite: 'ApplicationPoolIdentity'
            - task: IISWebAppDeploymentOnMachineGroup@0
              displayName: 'Deploy Trystler API'
              inputs:
                WebSiteName: 'trystlerApi'
                Package: '$(Build.ArtifactStagingDirectory)\..\**\webapi.zip'
                RemoveAdditionalFilesFlag: true
                TakeAppOfflineFlag: true
