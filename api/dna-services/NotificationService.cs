﻿using DateNightPersistence.Enums;
using DateNightPersistence.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightDataServices
{
    public class NotificationService
    {
        private readonly ILogger<NotificationService> _logger;
        readonly DateNightPersistence.DataContext _context;


        public NotificationService(DateNightPersistence.DataContext context, ILogger<NotificationService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public Notification GetById(string id)
        {
            _logger.LogInformation($"getting notification using id {id}");
            if (string.IsNullOrEmpty(id)) return null;
            Notification notification = new Notification();
            
            //parse key into long
            long keyLong;
            bool isValidLong = long.TryParse(id, out keyLong);
            //if long, get first vote record with id equal to key
            if (isValidLong) notification = _context.Notification.FirstOrDefault(u => u.Id.Equals(keyLong));

            //return vote
            return notification;
        }

        public IQueryable<Notification> GetQueryable()
        {
            _logger.LogInformation($"Get Notification Records as queryable");
            return _context.Notification.AsQueryable();
        }

        public IEnumerable<Notification> GetRecordsByUserId(string userId, NotificationType? notificationType = null, NotificationStatus? notificationStatus = null) {
            try
            {
                _logger.LogInformation($"Get Notification records by userId: {userId}. notificationType {notificationType}, notificationStatus {notificationStatus}");
                if (string.IsNullOrEmpty(userId)) return null;
                Guid keyGuid;
                bool isValidGuid = Guid.TryParse(userId, out keyGuid);

                if (isValidGuid)
                {
                    IEnumerable<Notification> notifications = GetQueryable().Where(x => x.UserId.Equals(keyGuid)).OrderByDescending(x => x.Id);
                    if (notificationType != null)
                    {
                        notifications = notifications.Where(x => x.NotificationStatus.Equals(notificationType));
                    }
                    if (notificationStatus != null)
                    {
                        notifications = notifications.Where(x => x.NotificationStatus.Equals(notificationStatus));
                    }
                    else
                    {
                        notifications = notifications.Where(x => !x.NotificationStatus.Equals(NotificationStatus.Dismissed));
                    }
                    return notifications;
                }
                else
                {
                    throw new Exception("Invalid userId. UserId is not a GUID");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not obtain records  by userId: {userId}. notificationType {notificationType}, notificationStatus {notificationStatus}");
                return null;
            }
        }

        public IEnumerable<Notification> ViewNotificationsByUserId(string userId)
        {
            try
            {
                _logger.LogInformation($"Get Notification records by userId: {userId}.");
                if (string.IsNullOrEmpty(userId)) return null;
                Guid keyGuid;
                bool isValidGuid = Guid.TryParse(userId, out keyGuid);

                if (isValidGuid)
                {
                    IEnumerable<Notification> notifications = GetQueryable()
                        .Where(x => x.UserId.Equals(keyGuid) && x.NotificationStatus.Equals(NotificationStatus.NotViewed))
                        .ToList();

                    foreach (var n in notifications)
                    {
                        n.NotificationStatus = NotificationStatus.Viewed;
                        _context.Update(n);
                    }
                    _context.SaveChanges();

                    return notifications.Distinct();
                }
                else
                {
                    throw new Exception("Invalid userId. UserId is not a GUID");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not obtain records  by userId: {userId}.");
                return null;
            }
        }

        public void InsertNotification(Notification notification, ref bool isSuccess, ref string message)
        {
            try
            {
                Notification notificationInDb = IsExistingNotDismissedNotification(notification);
                if (notificationInDb != null)
                {
                    _logger.LogWarning($"Notification already exists in the database for the record in question {notification.RecordId} for user {notification.UserId}");
                    message = $"Notification already exists in the database for the record in question {notification.RecordId} for user {notification.UserId}";
                    return;
                }
                var recordInDb = IsExistingRecordInDb(notification);
                if (recordInDb == null)
                {
                    throw new Exception($"linked record id {notification.RecordId} does not exist in table corresponding with Table type {notification.TableType}");
                }

                notification.User = _context.Users.Find(notification.UserId);
                notification.CreatedDate = DateTime.Now;
                notification.CreatedBy = notification.User.DisplayName;
                try
                {
                    _context.Notification.Add(notification);
                    _context.SaveChanges();
                    message = $"notification with id {notification.Id} added with status {notification.NotificationStatus}";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to insert");
                }
            }
            catch (Exception e) {
                isSuccess = false;
                message = e.Message;
                _logger.LogError(e, "Failed to insert");
            }

        }

        private object IsExistingRecordInDb(Notification notification)
        {
            long keyLong;
            bool isValidLong = long.TryParse(notification.RecordId, out keyLong);
            Guid keyGuid;
            bool isValidGuid = Guid.TryParse(notification.RecordId, out keyGuid);
            switch (notification.TableType) {
                case TableType.Friend:
                    return _context.Friend.Find(keyLong);
                case TableType.FriendRequest:
                    return _context.FriendRequest.Find(keyLong);                    
                //case TableType.Item:
                  //  return _context.Items.Find(keyLong);                    
                case TableType.User:
                    return _context.Users.Find(keyGuid);
                case TableType.Vote:
                    return _context.Votes.Find(keyLong);
                case TableType.VoteSession:
                    return _context.VoteSession.Find(keyLong);
            }
            return null;

        }

        public Notification IsExistingNotDismissedNotification(Notification notification)
        {
            var notificationInDb = GetQueryable()
                .Where(x =>
                x.UserId.Equals(notification.UserId) &&
                x.NotificationType.Equals(notification.NotificationType) &&
                x.TableType.Equals(notification.TableType) &&
                x.RecordId.Equals(notification.RecordId) &&
                x.Message.Equals(notification.Message) &&
                !x.NotificationStatus.Equals(NotificationStatus.Dismissed)
                ).FirstOrDefault();
            return notificationInDb;
        }

        public void UpdateNotification(Notification notification, ref bool isSuccess, ref string message)
        {
            try
            {
                Notification notificationInDb = GetById(notification.Id.ToString());
                if (notificationInDb == null)
                    throw new Exception($"Could not find a notification with id {notification.Id}");

                notification.User = _context.Users.Find(notification.UserId);
                Notification existingNotificationMatchingChanges = IsExistingNotDismissedNotification(notification);
                if (existingNotificationMatchingChanges != null && existingNotificationMatchingChanges.Id != notification.Id)
                    throw new Exception($"Notification already exists in the database for the record in question {notification.RecordId} for user {notification.UserId}");
                var recordInDb = IsExistingRecordInDb(notification);
                if (recordInDb == null)
                    throw new Exception($"linked record id {notification.RecordId} does not exist in table corresponding with Table type {notification.TableType}");

                try
                {
                    notificationInDb.Message = notification.Message;
                    notificationInDb.NotificationType = notification.NotificationType;
                    notificationInDb.TableType = notification.TableType;
                    notificationInDb.RecordId = notification.RecordId;
                    notificationInDb.UserId = notification.UserId;
                    notificationInDb.User = notification.User;
                    notificationInDb.UpdatedDate = DateTime.Now;
                    notificationInDb.UpdatedBy = notification.User.DisplayName;
                    _context.SaveChanges();
                    message = $"notification with id {notification.Id} updated";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to update");
                }
            }
            catch (Exception e)
            {
                isSuccess = false;
                message = e.Message;
                _logger.LogError(e, "Failed to update");
            }
        }

        public void UpdateNotificationStatus(string notificationId, NotificationStatus status, ref bool isSuccess, ref string message)
        {
            try {
                _logger.LogInformation($"Getting notification record with id {notificationId}");
                Notification notification = GetById(notificationId);
                
                if (notification == null)
                    throw new Exception($"Could not find a notification with id {notificationId}");
                if (notification.NotificationStatus.Equals(NotificationStatus.Dismissed)) {
                    Notification existingNotificationMatchingChanges = IsExistingNotDismissedNotification(notification);
                    if (existingNotificationMatchingChanges != null)
                        throw new Exception($"Notification already exists in the database for the record in question {notification.RecordId} for user {notification.UserId} that is not dismissed");
                }
                

                notification.NotificationStatus = status;
                notification.UpdatedDate = DateTime.Now;
                notification.UpdatedBy = notification.User.DisplayName;
                try
                {
                    _context.SaveChanges();
                    message = $"notification with id {notificationId} updated successfully to status {status}";
                    _logger.LogInformation(message);
                }
                catch (Exception e) {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to update");
                }

            } catch (Exception e) {
                isSuccess = false;
                message = e.Message;
                _logger.LogError(e, "Failed to update");
            }
        }

        public void DismissUserNotifications(string userId, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"Dismissing notification records for user id {userId}");
                try
                {
                    var userNotifications = GetRecordsByUserId(userId);

                    foreach (var n in userNotifications)
                    {
                        n.NotificationStatus = NotificationStatus.Dismissed;
                        n.UpdatedDate = DateTime.Now;
                        n.UpdatedBy = n.User.DisplayName;
                    }
                
                    _context.SaveChanges();
                    message = $"All notifications for user id {userId} dismissed";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to update");
                }

            }
            catch (Exception e)
            {
                isSuccess = false;
                message = e.Message;
                _logger.LogError(e, "Failed to update");
            }
        }
    }
}
