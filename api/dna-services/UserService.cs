﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using System.Linq;
using DateNightPersistence.Enums;
using Microsoft.EntityFrameworkCore;

namespace DateNightDataServices
{
    public class UserService
    {
        private readonly ILogger<UserService> _logger;
        readonly DateNightPersistence.DataContext _context;


        public UserService(DateNightPersistence.DataContext context, ILogger<UserService> logger) {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Get a user object based on passed in key
        /// </summary>
        /// <param name="key">string used to id user objects</param>
        /// <returns>User object</returns>
        public User Get(string key) {
            //if string is null return null
            _logger.LogInformation($"getting user using key {key}");
            if (string.IsNullOrEmpty(key)) return null;
            //try to get first user with email equal to key
            User user = _context.Users.FirstOrDefault(u => u.Email == key || u.PhoneNumber == key || u.DisplayName == key || u.UserId.ToString() == key);
            //try to parse key as GUID
            Guid keyGuid;
            bool isValidGuid = Guid.TryParse(key, out keyGuid);
            //if user is null and key is a GUID, get first user with id equal to key
            if (user == null && isValidGuid) user = _context.Users.FirstOrDefault(u => u.UserId.Equals(keyGuid));

            //return user
            return user;
        }


        /// <summary>
        /// Get queryable list of user
        /// </summary>
        /// <returns>queryable list of user</returns>
        public IQueryable<User> GetQueryable() {
            return _context.Users.AsQueryable();
        }

        public bool IsExistingUserwithPhoneNumber(string phoneNumber)
        {
            User user = _context.Users.FirstOrDefault(x => x.PhoneNumber == phoneNumber);
            if (user == null) { return false; } else { return true; }
        }

        /// <summary>
        /// try to insert a user in the database
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="isSuccess">success flag reference for call</param>
        /// <param name="message">Message flag reference for call</param>
        public void InsertUser(User user, ref bool isSuccess, ref string message)
        {
            try {
                //try to find user in db with email
                User userInDb = Get(user.DisplayName);
                //if null
                if (userInDb == null)
                {
                    //add user
                    user.CreatedDate = DateTime.Now;
                    user.CreatedBy = user.DisplayName;
                    user.Email ??= String.Empty;
                    user.FirstName ??= String.Empty;
                    user.LastName ??= String.Empty;
                    _context.Users.Add(user);
                    _context.SaveChanges();
                    message = "Inserted user successfully";
                    _logger.LogInformation($"{message}");
                }
                else {
                    //return false for success flag if not null
                    isSuccess = false;
                    message = "A user already exists";
                    _logger.LogError($"{message}");
                }
            }

            catch (Exception ex) {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
                
            }
        }

        public bool IsExistingUserwithEmail(string email)
        {
            if (String.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            return _context.Users.Any(x => x.Email == email);
        }

        public bool DeleteUser(User user, ref bool isSuccess, ref string message)
        {
            try
            {
                //try to find user in db with email
                User userInDb = Get(user.UserId.ToString());
                //if null
                if (userInDb == null)
                {
                    //return false for success flag if null
                    isSuccess = false;
                    message = "A user does not exist";
                    _logger.LogError($"{message}");
                    return false;
                }
                else
                {
                    _logger.LogInformation("Collecting User Data");
                    var userNotifications = _context.Notification.Where(f => f.UserId == user.UserId);
                    var userFriendships = _context.Friend.Where(f => f.UserId == user.UserId || f.UserFriendId == user.UserId);
                    var userFriendRequests = _context.FriendRequest.Where(f => f.SenderId == user.UserId || f.RecipientId == user.UserId);
                    var userFilters = _context.Filter.Where(f => f.UserId == user.UserId);
                    var userHangouts = _context.VoteSession.Where(vs => vs.AUserId == user.UserId || vs.BUserId == user.UserId);
                    var userHangoutVotes = _context.Votes.Where(v => v.UserId == user.UserId || userHangouts.Any(vs => vs.Id == v.VoteSessionId));
                    var userHangoutFilters = _context.VoteSessionFilter.Where(vsf => userHangouts.Any(vs => vs.Id == vsf.VoteSessionId) || userFilters.Any(uf => uf.Id == vsf.FilterId));

                    _logger.LogInformation("Clearing User Data");
                    _context.Notification.RemoveRange(userNotifications);
                    _context.Friend.RemoveRange(userFriendships);
                    _context.FriendRequest.RemoveRange(userFriendRequests);
                    _context.SaveChanges();

                    _logger.LogInformation("Clearing Hangouts and Peripheral Data");
                    _context.Votes.RemoveRange(userHangoutVotes);
                    _context.VoteSessionFilter.RemoveRange(userHangoutFilters);
                    _context.Filter.RemoveRange(userFilters);
                    _context.VoteSession.RemoveRange(userHangouts);
                    _context.SaveChanges();

                    _logger.LogInformation("Removing User");
                    _context.Users.Remove(userInDb);
                    _context.SaveChanges();

                    message = "Deleted user successfully";
                    _logger.LogInformation(message);
                    return true;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to delete");
                return false;
            }
        }

        public bool IsExistingUserwithDisplayName(string displayName)
        {
            User user = _context.Users.FirstOrDefault(x => x.DisplayName == displayName);
            if (user == null) { return false; } else { return true; }
        }

        /// <summary>
        /// Try to update an existing user record
        /// </summary>
        /// <param name="user">user object to update</param>
        /// <param name="isSuccess">success flag refernece for call</param>
        /// <param name="message">message reference for call</param>
        public void UpdateUser(User user, ref bool isSuccess, ref string message)
        {
            try
            {
                //try to find user in db with email
                User userInDb = Get(user.UserId.ToString());
                //if null
                if (userInDb == null)
                {
                    //return false for success flag if null
                    isSuccess = false;
                    message = "A user does not exist";
                    _logger.LogError($"{message}");
                }
                else
                {
                    //update if not null
                    userInDb.DisplayName = user.DisplayName;
                    userInDb.Email = user.Email;
                    userInDb.PhoneNumber = user.PhoneNumber;
                    userInDb.FirstName = user.FirstName;
                    userInDb.LastName = user.LastName;
                    userInDb.UpdatedBy = user.Email;
                    userInDb.Active = user.Active;
                    userInDb.UpdatedDate = DateTime.Now;
                    _context.SaveChanges();

                    message = "Updated user successfully";
                    _logger.LogInformation($"message");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogInformation("Failed to update");
            }
        }
    }
}
