﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using System.Linq;
using DateNightPersistence.Enums;

namespace DateNightDataServices
{
    public class FriendService
    {
        private readonly ILogger<FriendService> _logger;
        readonly DateNightPersistence.DataContext _context;
        private readonly VoteSessionService _voteSessionService;

        public FriendService(DateNightPersistence.DataContext context, ILogger<FriendService> logger, VoteSessionService voteSessionService)
        {
            _context = context;
            _logger = logger;
            _voteSessionService = voteSessionService;
        }

        public Friend Get(string key)
        {
            _logger.LogInformation($"getting friend record using key {key}");
            if (string.IsNullOrEmpty(key)) return null;

            Friend friend = _context.Friend.FirstOrDefault(u => u.FriendCode.Equals(key) || u.UserFriendIdentifier.Equals(key));

            long keyLong;

            bool isValidLong = long.TryParse(key, out keyLong);

            if (friend == null && isValidLong) friend = _context.Friend.FirstOrDefault(u => u.Id.Equals(keyLong));

            return friend;
        }

        public IQueryable<Friend> GetQueryable()
        {
            return _context.Friend.AsQueryable();
        }

        public void InsertFriend(Friend friend, ref bool isSuccess, ref string message)
        {
            try
            {
                Friend friendRecInDb = Get(friend.FriendCode);
                if (friendRecInDb is not null)
                {
                    //return false for success flag if not null
                    isSuccess = false;
                    message = "A friend record already exists";
                    _logger.LogError($"{message}");
                }

                var friendshipExists = _context.Friend.Any(f =>
                    (f.FriendStatus == FriendStatus.Accepted || f.FriendStatus == FriendStatus.Blocked) &&
                    (f.UserId == friend.UserId || f.UserFriendId == friend.UserId) &&
                    (f.UserId == friend.UserFriendId || f.UserFriendId == friend.UserFriendId)
                );
                if (friendshipExists)
                {
                    return;
                }

                IQueryable<Friend> friendInDb = _context.Friend.Where(f =>
                    (f.UserId.Equals(friend.UserId) && f.UserFriendId.Equals(friend.UserFriendId)) ||
                    (f.UserId.Equals(friend.UserFriendId) && f.UserFriendId.Equals(friend.UserId))
                    ).AsQueryable();

                var existingFriendRejection = friendInDb.FirstOrDefault(f =>
                    f.FriendStatus.Equals(FriendStatus.Declined)
                    || f.FriendStatus.Equals(FriendStatus.Cancelled)
                    || f.FriendStatus.Equals(FriendStatus.Unfriended));
                if (existingFriendRejection != null)
                {
                    isSuccess = false;
                    message = $"Friend Record Cannot be Made. There is an existing friend record in the {existingFriendRejection.FriendStatus} status";
                    _logger.LogError(message);
                }

                friend.CreatedBy = friend.User.DisplayName;
                friend.CreatedDate = DateTime.Now;
                _context.Friend.Add(friend);

                if (!String.IsNullOrWhiteSpace(friend?.FriendCode))
                {
                    ClaimFriendRequest(friend, saveChanges: false);
                }

                _context.SaveChanges();
                message = "Inserted friend successfully";
                _logger.LogInformation($"{message}");
            } 
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }

        private void ClaimFriendRequest(Friend friend, bool saveChanges = true)
        {
            var friendRequest = _context.FriendRequest.FirstOrDefault(fr => fr.ShareKey.ToString() == friend.FriendCode);
            if (friendRequest == null)
            {
                return;
            }

            friendRequest.Recipient = friend.UserFriend;
            _context.FriendRequest.Update(friendRequest);

            if (saveChanges)
            {
                _context.SaveChanges();
            }
        }

        public void UpdateFriend(Friend friend, User updater, ref bool isSuccess, ref string message)
        {
            try { 
                Friend friendInDb = Get(friend.Id.ToString());
                if (friendInDb == null) {
                    isSuccess = false;
                    message = "A friend record does not exist";
                    _logger.LogError($"{message}");
                }
                else {
                    friendInDb.UpdatedBy = updater.DisplayName;
                    friendInDb.UpdatedDate = DateTime.Now;
                    friendInDb.UserFriendId = friend.UserFriendId;
                    friendInDb.UserFriend = friend.UserFriend;
                    friendInDb.FriendStatus = friend.FriendStatus;
                    _context.SaveChanges();
                    message = "Inserted friend successfully";
                    _logger.LogInformation($"{message}");
                    if (friend.FriendStatus == FriendStatus.Unfriended) {
                        _voteSessionService.CancelVoteSessionsBetweenUsers(friendInDb.UserFriendId, friendInDb.UserId, updater, ref isSuccess, ref message);
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }

        public void UpdateFriendRecords(User user, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"Getting friend records with statuts {FriendStatus.Invited} and userFriendIdentifier {user.DisplayName}");
                var friendRecords = _context.Friend.Where(x => (x.UserFriendIdentifier == user.DisplayName || x.UserFriendIdentifier == user.PhoneNumber) && x.FriendStatus == FriendStatus.Invited).ToList();

                foreach (Friend rec in friendRecords) {
                    _logger.LogInformation($"Updating friend record with id {rec.Id}");
                    Friend recToUpdate = _context.Friend.Find(rec.Id);
                    recToUpdate.UserFriendId = user.UserId;
                    recToUpdate.FriendStatus = FriendStatus.Requested;
                    recToUpdate.UpdatedDate = DateTime.Now;
                    recToUpdate.UpdatedBy = user.DisplayName;
                    
                }
                message = "Friend records updated successfully";
                _context.SaveChanges();
                _logger.LogInformation($"{message}");
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to update");
            }
        }
    }
}
