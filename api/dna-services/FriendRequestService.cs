﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using System.Linq;
using DateNightPersistence.Enums;
using Newtonsoft.Json;

namespace DateNightDataServices
{
    public class FriendRequestService
    {
        private readonly ILogger<FriendRequestService> _logger;
        readonly DateNightPersistence.DataContext _context;


        public FriendRequestService(DateNightPersistence.DataContext context, ILogger<FriendRequestService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public FriendRequest GetFriendRequest(Guid senderId, Guid? recipientId = null)
        {
            var friendRequest = _context.FriendRequest.FirstOrDefault(r => r.SenderId == senderId && r.RecipientId == recipientId);
            if (friendRequest == null)
            {
                friendRequest = new FriendRequest
                {
                    SenderId = senderId,
                    RecipientId = recipientId,
                    ShareKey = Guid.NewGuid(),
                };
                _context.FriendRequest.Add(friendRequest);
                _context.SaveChanges();
            }
            return friendRequest;
        }

        public FriendRequest GetFriendRequestRecordByShareKey(Guid shareKey)
        {
            _logger.LogInformation($"getting friend request record using key {shareKey}");
            return _context.FriendRequest.FirstOrDefault(x => x.ShareKey.Equals(shareKey));
        }

        public void InsertFriendRequest(FriendRequest friendRequest, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"getting friend request record using key {friendRequest.ShareKey}");
                FriendRequest fReqInDb = GetFriendRequestRecordByShareKey(friendRequest.ShareKey);
                if (fReqInDb == null)
                {
                    if (friendRequest.RecipientId != null)
                    {
                        IQueryable<Friend> FriendRequestInDb = _context.Friend.Where(f =>
                        (f.UserId.Equals(friendRequest.SenderId) && f.UserFriendId.Equals(friendRequest.RecipientId)) ||
                        (f.UserId.Equals(friendRequest.RecipientId) && f.UserFriendId.Equals(friendRequest.SenderId))
                        ).AsQueryable();
                        _logger.LogInformation($"Recipient not null, checking for pairing in Friend with blocked status");
                        if (FriendRequestInDb.Any(f => f.FriendStatus.Equals(FriendStatus.Blocked)))
                        {
                            isSuccess = false;
                            message = "A Friend Request cannot be created";
                            _logger.LogError($"The friend request pairing cannot be created. Friend status is blocked");
                            return;
                        }
                    }
                    _logger.LogInformation($"Inserting Friend Request record");
                    friendRequest.CreatedBy = friendRequest.Sender.DisplayName;
                    friendRequest.CreatedDate = DateTime.Now;
                    _context.FriendRequest.Add(friendRequest);
                    _context.SaveChanges();
                    message = "Inserted friend request successfully";
                    _logger.LogInformation($"{message}");
                    
                }
                else {
                    isSuccess = false;
                    message = "A Friend Request with the share key already exists";
                    _logger.LogError($"A Friend Request with the share key {friendRequest.ShareKey} already exists");
                }
            }
            catch (Exception ex) {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to insert");
            }
        }
        public void UpdateFriendRequest(FriendRequest friendRequest, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"getting friend request record using key {friendRequest.ShareKey}");
                FriendRequest fReqInDb = GetFriendRequestRecordByShareKey(friendRequest.ShareKey);
                if (fReqInDb != null)
                {
                    if (friendRequest.RecipientId != null)
                    {
                        IQueryable<Friend> FriendRequestInDb = _context.Friend.Where(f =>
                        (f.UserId.Equals(fReqInDb.SenderId) && f.UserFriendId.Equals(fReqInDb.RecipientId)) ||
                        (f.UserId.Equals(fReqInDb.RecipientId) && f.UserFriendId.Equals(fReqInDb.SenderId))
                        ).AsQueryable();
                        if (FriendRequestInDb.Any(f => f.FriendStatus.Equals(FriendStatus.Blocked)))
                        {
                            isSuccess = false;
                            message = "A Friend Request cannot be updated";
                            _logger.LogError($"The friend request pairing cannot be updated. Friend status is blocked");
                            return;
                        }
                    }
                    _logger.LogInformation($"Updating Friend Request record");
                    fReqInDb.UpdatedBy = friendRequest.Recipient.DisplayName;
                    fReqInDb.UpdatedDate = DateTime.Now;
                    fReqInDb.RecipientId = friendRequest.RecipientId;
                    fReqInDb.Recipient = friendRequest.Recipient;
                    
                    _context.SaveChanges();
                    message = "Updated friend request successfully";
                    _logger.LogInformation($"{message}");

                }
                else
                {
                    isSuccess = false;
                    message = "A Friend Request with the share key does not exists";
                    _logger.LogError($"A Friend Request with the share key {friendRequest.ShareKey} does not exist");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to update");
            }
        }
    }
}
