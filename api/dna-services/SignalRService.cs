﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DateNightDataServices
{
    public class SignalRService
    {
        private readonly ServerHandler _signalR;
        public SignalRService(IConfiguration configuration)
        {
            _signalR = new ServerHandler(configuration["SignalR:ConnectionString"]);
        }

        /// <summary>
        /// Send a SingalR Message to a given Hub Method, targeting a User Id
        /// </summary>
        /// <param name="method"></param>
        /// <param name="hubName"></param>
        /// <param name="userId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendMessageAsync(string hubName, string method, string userId, string message)
        {
            await _signalR.SendRequest(new HubMessage
            {
                HubName = hubName,
                Method = method,
                UserId = userId,
                Message = message,
            });
        }

        /// <summary>
        /// Send a SingalR Message to a given Hub Method
        /// </summary>
        /// <param name="hubMessage">Parameter Class containing string properties: HubName, Method, UserId, and Message</param>
        /// <returns></returns>
        public async Task SendMessageAsync(HubMessage hubMessage)
        {
            await _signalR.SendRequest(hubMessage);
        }
    }
    public class HubMessage
    {
        /// <summary>
        /// Hub through which the signal is sent
        /// </summary>
        public string HubName { get; set; }
        /// <summary>
        /// Method to trigger
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// UserId of User to send this signal to
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// An informational message, or a JSON string to be deserialized app-side
        /// </summary>
        public string Message { get; set; }
    }

    #region Server Connection Handler/Jwt Utils
    public class ServerHandler
    {
        private static readonly HttpClient Client = new HttpClient();
        private readonly string _serverName;
        private readonly ServiceUtils _serviceUtils;
        private readonly string _endpoint;

        public ServerHandler(string connectionString)
        {
            _serverName = "Trystler";
            _serviceUtils = new ServiceUtils(connectionString);
            _endpoint = _serviceUtils.Endpoint;
        }

        public async Task SendRequest(HubMessage hubMessage)
        {
            string url = $"{_endpoint}/api/v1/hubs/{hubMessage.HubName.ToLower()}/users/{hubMessage.UserId}";

            if (!string.IsNullOrEmpty(url))
            {
                var request = BuildRequest(url, hubMessage);

                using (var response = await Client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead))
                {
                    if (response.StatusCode != HttpStatusCode.Accepted)
                    {
                        Console.WriteLine($"Sent error: {response.StatusCode}");
                    }
                }
            }
        }

        private HttpRequestMessage BuildRequest(string url, HubMessage hubMessage)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new UriBuilder(url).Uri);

            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", _serviceUtils.GenerateAccessToken(url, _serverName));
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var json = JsonConvert.SerializeObject(new { 
                Target = hubMessage.Method, 
                Arguments = new[] { hubMessage.HubName, hubMessage.Message } 
            });
            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            return request;
        }
    }

    public class ServiceUtils
    {
        private static readonly JwtSecurityTokenHandler JwtTokenHandler = new JwtSecurityTokenHandler();

        public string Endpoint { get; }

        public string AccessKey { get; }

        public ServiceUtils(string connectionString)
        {
            (Endpoint, AccessKey) = ParseConnectionString(connectionString);
        }

        public string GenerateAccessToken(string audience, string userId, TimeSpan? lifetime = null)
        {
            IEnumerable<Claim> claims = null;
            if (userId != null)
            {
                claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userId)
                };
            }

            return GenerateAccessTokenInternal(audience, claims, lifetime ?? TimeSpan.FromHours(1));
        }

        public string GenerateAccessTokenInternal(string audience, IEnumerable<Claim> claims, TimeSpan lifetime)
        {
            var expire = DateTime.UtcNow.Add(lifetime);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AccessKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = JwtTokenHandler.CreateJwtSecurityToken(
                issuer: null,
                audience: audience,
                subject: claims == null ? null : new ClaimsIdentity(claims),
                expires: expire,
                signingCredentials: credentials);
            return JwtTokenHandler.WriteToken(token);
        }

        private static readonly char[] PropertySeparator = { ';' };
        private static readonly char[] KeyValueSeparator = { '=' };
        private const string EndpointProperty = "endpoint";
        private const string AccessKeyProperty = "accesskey";

        internal static (string, string) ParseConnectionString(string connectionString)
        {
            var properties = connectionString.Split(PropertySeparator, StringSplitOptions.RemoveEmptyEntries);
            if (properties.Length > 1)
            {
                var dict = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                foreach (var property in properties)
                {
                    var kvp = property.Split(KeyValueSeparator, 2);
                    if (kvp.Length != 2) continue;

                    var key = kvp[0].Trim();
                    if (dict.ContainsKey(key))
                    {
                        throw new ArgumentException($"Duplicate properties found in connection string: {key}.");
                    }

                    dict.Add(key, kvp[1].Trim());
                }

                if (dict.ContainsKey(EndpointProperty) && dict.ContainsKey(AccessKeyProperty))
                {
                    return (dict[EndpointProperty].TrimEnd('/'), dict[AccessKeyProperty]);
                }
            }

            throw new ArgumentException($"Connection string missing required properties {EndpointProperty} and {AccessKeyProperty}.");
        }
    }
    #endregion
}
