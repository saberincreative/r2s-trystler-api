﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using DateNightPersistence.Enums;
using System.Linq;
using System.Collections.Generic;

namespace DateNightDataServices
{
    public class VoteService
    {
        private readonly ILogger<VoteService> _logger;
        readonly DateNightPersistence.DataContext _context;


        public VoteService(DateNightPersistence.DataContext context, ILogger<VoteService> logger)
        {
            _context = context;
            _logger = logger;
        }
        /// <summary>
        /// Get vote record by key passed in
        /// </summary>
        /// <param name="key">id associated with vote object</param>
        /// <returns>vote object</returns>
        public Vote Get(string key)
        {
            _logger.LogInformation($"Get vote records using the key {key}");
            //if key is null return null
            if (string.IsNullOrEmpty(key)) return null;
            Vote vote = new Vote();
            //VoteSession voteSession = _context.VoteSession.FirstOrDefault(u => u.Text == key);
            //parse key into long
            long keyLong;
            bool isValidLong = long.TryParse(key, out keyLong);
            //if long, get first vote record with id equal to key
            if (isValidLong) vote = _context.Votes.FirstOrDefault(u => u.Id.Equals(keyLong));

            //return vote
            return vote;
        }

        /// <summary>
        /// get queryable list of votes
        /// </summary>
        /// <returns>list of votes</returns>
        public IQueryable<Vote> GetQueryable()
        {
            _logger.LogInformation($"Get Vote Records as queryable");
            return _context.Votes.AsQueryable();
        }

        /// <summary>
        /// Try to insert a list of vote objects
        /// </summary>
        /// <param name="userVotes">collection of vote objects to insert</param>
        /// <param name="user">user object inserting</param>
        /// <param name="isSuccess">success flag reference for insert</param>
        /// <param name="message">message reference for call</param>
        public void InsertUserVotes(ICollection<Vote> userVotes, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"Inserting user votes");
                //gets associated session id
                long sessionId = userVotes.ToList()[0].VoteSessionId;

                //gets session object using session id
                VoteSession session = _context.VoteSession.FirstOrDefault(s => s.Id.Equals(sessionId));
                _logger.LogInformation($"Obtained vote session {session.Id}");
                //loop through all votes associated with session object
                _logger.LogInformation($"Marking all votes associated with session as voted on");
                foreach (Vote vote in session.Votes)
                {
                    
                    //set voted on to true
                    vote.VotedOn = true;
                    vote.UpdatedDate = DateTime.Now;
                    vote.UpdatedBy = user.DisplayName;

                }

                //loop through passed in user votes
                foreach (Vote newVotes in userVotes)
                {
                    _logger.LogInformation($"inserting new vote for item id {newVotes.ItemId} and session {newVotes.VoteSessionId} voted on by user {newVotes.UserId}");
                    //add votes
                    newVotes.CreatedBy = user.DisplayName;
                    newVotes.CreatedDate = DateTime.Now;
                    newVotes.VotedOn = false;
                    _context.Votes.Add(newVotes);
                }
                //save changes
                message = "Votes updated successfully";
                _logger.LogInformation($"{message}");
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }

        /// <summary>
        /// The method sets voted on to true for all votes associated with session (behavior for when no to all is selected)
        /// </summary>
        /// <param name="session">session object</param>
        /// <param name="user">user object</param>
        /// <param name="isSuccess">success flag</param>
        /// <param name="message">message</param>
        public void SetVotedOnForVotesInVoteSession(VoteSession session, User user, ref bool isSuccess, ref string message)
        {
            try {
                //get id associated with vote session
                long sessionId = session.Id;


                //gets session object using session id
                VoteSession sessionInDb = _context.VoteSession.FirstOrDefault(s => s.Id.Equals(sessionId));
                _logger.LogInformation($"Obtained Vote session {sessionInDb.Id}");
                _logger.LogInformation($"Setting voted on to true for all votes associated with vote session {sessionInDb.Id}");
                //loop through all votes associated with session object
                foreach (Vote vote in sessionInDb.Votes)
                {
                    //set voted on to true
                    vote.VotedOn = true;
                    vote.UpdatedDate = DateTime.Now;
                    vote.UpdatedBy = user.DisplayName;

                }
                //save changes
                message = "Votes updated successfully";
                _logger.LogInformation($"{message}");
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }

        }

        public void UpdateVoteStatus(string voteId, string voteStatus, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                //try to find user in db with email
                Vote voteInDb = Get(voteId);
                //if null
                if (voteInDb == null)
                {
                    //return false for success flag if null
                    isSuccess = false;
                    message = "Vote does not exist";
                    _logger.LogError($"{message}");

                }
                else
                {
                    _logger.LogInformation($"Setting votestatus for vote {voteId} to {voteStatus}");
                    VoteStatus to = VoteStatus.None;
                    int statusInt;
                    bool isValidInt = int.TryParse(voteStatus, out statusInt);
                    if (!isValidInt)
                    {
                        isSuccess = false;
                        message = "Incorrect vote status passed";
                        _logger.LogError($"{message}");
                        return;
                    }
                    else {
                        switch (statusInt) {
                            case 0:
                                to = VoteStatus.None;
                                break;
                            case 100:
                                to = VoteStatus.Cancelled;
                                break;
                            case 200:
                                to = VoteStatus.Submitted;
                                break;
                            case 300:
                                to = VoteStatus.Completed;
                                break;
                            default:
                                isSuccess = false;
                                message = "Incorrect vote status passed";
                                _logger.LogError($"{message}");
                                return;
                        }
                    }
                    //update if not null

                    voteInDb.UpdatedBy = user.DisplayName;
                    voteInDb.UpdatedDate = DateTime.Now;
                    voteInDb.VoteStatus = to;
                    _context.SaveChanges();

                    message = "Updated user successfully";
                    _logger.LogInformation($"{message}");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }
    }
}
