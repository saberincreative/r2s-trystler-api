﻿using DateNightPersistence.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightDataServices
{
    public class VoteSessionFilterService
    {
        private readonly ILogger<VoteSessionFilterService> _logger;

        readonly DateNightPersistence.DataContext _context;
        public VoteSessionFilterService(DateNightPersistence.DataContext context, ILogger<VoteSessionFilterService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void InsertVoteSessionFilter(VoteSessionFilters voteSessionFilter, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"{user?.DisplayName} Trying to insert a vote session filter");
                _logger.LogInformation("Checking db for existing vote session filter record that matches new record");
                VoteSessionFilters vsfInDb = _context.VoteSessionFilter.Where(vsf => vsf.FilterId == voteSessionFilter.FilterId && vsf.VoteSessionId == voteSessionFilter.VoteSessionId).FirstOrDefault();
                if (vsfInDb != null)
                {
                    message = $"Vote Session Filter with similar properties already exists";
                    throw new Exception(message);
                }
                _logger.LogInformation("No duplicates found, proceeding with insert");
                voteSessionFilter.CreatedDate = DateTime.Now;
                voteSessionFilter.CreatedBy = user?.DisplayName == null ? "unknown" : user.DisplayName;
                voteSessionFilter.Filter = _context.Filter.Find(voteSessionFilter.FilterId);
                voteSessionFilter.VoteSession = _context.VoteSession.Find(voteSessionFilter.VoteSessionId);
                try
                {
                    _context.Add(voteSessionFilter);
                    _context.SaveChanges();
                    message = $"Successfully added new vote session filter with id {voteSessionFilter.Id}";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to insert");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to insert filter");
            }
        }

        public void UpdateVoteSessionFilter(VoteSessionFilters voteSessionFilter, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"{user?.DisplayName} Trying to update a vote session filter");
                _logger.LogInformation("Checking db for existing vote session filter record that matches record");
                VoteSessionFilters vsfInDb = _context.VoteSessionFilter.Find(voteSessionFilter.Id);
                if (vsfInDb == null)
                {
                    message = $"Vote Session Filter record does not exist";
                    throw new Exception(message);
                }
                VoteSessionFilters vsMatch = _context.VoteSessionFilter.Where(vsf => vsf.FilterId == voteSessionFilter.FilterId && vsf.VoteSessionId == voteSessionFilter.VoteSessionId).FirstOrDefault();
                if(vsMatch != null)
                {
                    message = $"Vote Session Filter with similar properties already exists";
                    throw new Exception(message);
                }
                _logger.LogInformation("Proceeding with update");

                try
                {
                    vsfInDb.UpdatedDate = DateTime.Now;
                    vsfInDb.UpdatedBy = user?.DisplayName == null ? "unknown" : user.DisplayName;
                    vsfInDb.Filter = _context.Filter.Find(voteSessionFilter.FilterId);
                    vsfInDb.FilterId = voteSessionFilter.FilterId;
                    vsfInDb.VoteSession = _context.VoteSession.Find(voteSessionFilter.VoteSessionId);
                    vsfInDb.VoteSessionId = voteSessionFilter.VoteSessionId;
                    _context.SaveChanges();
                    message = $"Successfully updated vote session filter with id {voteSessionFilter.Id}";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to update");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to update vote session filter");
            }
        }

        public void RemoveVoteSessionFilter(VoteSessionFilters voteSessionFilter, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"Searching db for vote session filter with id {voteSessionFilter.Id}");
                VoteSessionFilters vsfInDb = _context.VoteSessionFilter
                                        .Where(f => f.Id.Equals(voteSessionFilter.Id))
                                        .FirstOrDefault();
                if (vsfInDb == null)
                {
                    message = $"No vote session filter in database with id {voteSessionFilter.Id}";
                    throw new Exception(message);
                }
                _logger.LogInformation($"Removing identified vote session filter");
                _context.Remove(vsfInDb);
                _context.SaveChanges();
                message = $"Successfully removed filter with id {voteSessionFilter.Id}";
                _logger.LogInformation(message);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to remove vote session filter");
            }
        }

        public VoteSessionFilters Get(string key)
        {
            try
            {
                long validLong;
                bool isValidKey = long.TryParse(key, out validLong);
                if (isValidKey)
                {
                    return _context.VoteSessionFilter.Where(vsf => vsf.Id.Equals(validLong)).FirstOrDefault();
                }
                else
                {
                    throw new Exception($"Invalid key {key} is not a long");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get");
                return null;
            }
        }

        public IEnumerable<VoteSessionFilters> GetVoteSessionFilterRecordsByVoteSessionId(string voteSessionId)
        {
            try
            {
                long validLong;
                bool isValidKey = long.TryParse(voteSessionId, out validLong);
                if (isValidKey)
                {
                    return _context.VoteSessionFilter.Where(vsf => vsf.VoteSessionId.Equals(validLong));
                }
                else
                {
                    throw new Exception($"Invalid key {voteSessionId} is not a long");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get");
                return null;
            }
        }

        public IEnumerable<VoteSessionFilters> GetVoteSessionFilterRecordsByFilterId(string filterId)
        {
            try
            {
                long validLong;
                bool isValidKey = long.TryParse(filterId, out validLong);
                if (isValidKey)
                {
                    return _context.VoteSessionFilter.Where(vsf => vsf.FilterId.Equals(validLong));
                }
                else
                {
                    throw new Exception($"Invalid key {filterId} is not a long");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get");
                return null;
            }
        }
    }
}
