﻿using DateNightPersistence.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateNightDataServices
{
    public class FilterService
    {
        private readonly ILogger<FilterService> _logger;
        
        readonly DateNightPersistence.DataContext _context;
        public FilterService(DateNightPersistence.DataContext context, ILogger<FilterService> logger)
        {
            _context = context;
            _logger = logger;
          
        }

        public IQueryable<Filter> GetQueryable()
        {
            return _context.Filter.AsQueryable();
        }

        public Filter Get(string key, bool getDefault, string keyWord, string voteType, string filterType) {
            return GetEnumerable(key, getDefault, keyWord, voteType, filterType).FirstOrDefault();
        }

        public Filter GetByKeyWord(string keyWord)
        {
            return _context.Filter.FirstOrDefault(f => f.KeyWord.Equals(keyWord));
        }

        public IEnumerable<Filter> GetFilters(string key, bool getDefault, string keyWord, string voteType, string filterType)
        {
            return GetEnumerable(key, getDefault, keyWord, voteType, filterType);
        }

        public IEnumerable<Filter> GetEnumerable(string key, bool getDefault, string keyWord, string voteType, string filterType)
        {
            try
            {
                IEnumerable<Filter> filtersInDb = GetQueryable();
                int validVoteType;
                bool isValidVoteType = int.TryParse(voteType, out validVoteType);
                int validfilterType;
                bool isValidFilterType = int.TryParse(filterType, out validfilterType);
                if (!string.IsNullOrEmpty(key))
                {
                    Guid keyGuid;
                    long keyLong;
                    bool isKeyGuid = Guid.TryParse(key, out keyGuid);
                    bool isKeyLong = long.TryParse(key, out keyLong);

                    if (isKeyLong)
                    {
                        _logger.LogInformation($"Getting filter with id {keyLong}");
                        return _context.Filter.Where(f => f.Id.Equals(keyLong));
                    }
                    if (isKeyGuid)
                    {
                        _logger.LogInformation($"Getting filter with id {keyGuid}");
                        filtersInDb = filtersInDb.Where(f => f.UserId.Equals(keyGuid));
                    }
                }
                if (!string.IsNullOrEmpty(keyWord))
                {
                    _logger.LogInformation($"Getting filter with key word {keyWord}");
                    filtersInDb = filtersInDb.Where(f => f.KeyWord.Equals(keyWord));
                }
                if (isValidVoteType)
                {
                    _logger.LogInformation($"Getting filter with voteType {validVoteType}");
                    filtersInDb = filtersInDb.Where(f => (int)f.VoteType == validVoteType);
                }
                if (isValidFilterType)
                {
                    _logger.LogInformation($"Getting filter with filterType {validfilterType}");
                    filtersInDb = filtersInDb.Where(f => (int)f.FilterType == validfilterType);
                }
                if (getDefault)
                {
                    _logger.LogInformation($"Getting default filter");
                    filtersInDb = filtersInDb.Where(f => f.IsDefault == true);
                }
                return filtersInDb.OrderByDescending(f => f.Id);
            }
            catch (Exception e) {
                _logger.LogError(e, "Failed to get");
                return null;
            }

        }

        public void UpdateFilter(Filter filter, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"{user?.DisplayName} Trying to update a filter");
                _logger.LogInformation("Checking db if record exists");
                Filter filterInDb = _context.Filter.Find(filter.Id);
                if (filterInDb == null)
                {
                    message = $"Filter record does not exist";
                    throw new Exception(message);
                }
                Filter matchingFilter = Get(filter.UserId.ToString(), filter.IsDefault, filter.KeyWord, ((int)filter.VoteType).ToString(), ((int)filter.FilterType).ToString());
                if (matchingFilter != null || matchingFilter.Id != filter.Id)
                {
                    message = $"Filter with similar properties already exists";
                    throw new Exception(message);
                }
                _logger.LogInformation("Proceeding with update");
               
                try
                {
                    filterInDb.UpdatedDate = DateTime.Now;
                    filterInDb.UpdatedBy = user?.DisplayName == null ? "unknown" : user.DisplayName;
                    filterInDb.User = _context.Users.Find(filter.UserId);
                    filterInDb.UserId = filter.UserId;
                    filterInDb.VoteType = filter.VoteType;
                    filterInDb.KeyWord = filter.KeyWord;
                    filterInDb.IsDefault = filter.IsDefault;
                    filterInDb.FilterType = filter.FilterType;
                    _context.SaveChanges();
                    message = $"Successfully updated filter with id {filter.Id}";
                    _logger.LogInformation(message);
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to update");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to update filter");
            }
        }

        public void RemoveFilter(Filter filter, ref bool isSuccess, ref string message)
        {
            try
            {
                _logger.LogInformation($"Searching db for filter with id {filter.Id}");
                Filter filterInDb = _context.Filter
                                        .Include(f => f.VoteSessionFilters)
                                        .Where(f => f.Id.Equals(filter.Id))
                                        .FirstOrDefault();
                if (filterInDb == null) {
                    message = $"No filter in database with id {filter.Id}";
                    throw new Exception(message);
                }
                _logger.LogInformation($"Removing identified filter and associated vote session filters");
                _context.Remove(filterInDb);
                _context.SaveChanges();
                message = $"Successfully removed filter with id {filter.Id}";
                _logger.LogInformation(message);
            }
            catch (Exception ex) {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to remove filter");
            }
        }

        public Filter InsertFilter(Filter filter, User user, ref bool isSuccess, ref string message)
        {
            try {
                _logger.LogInformation($"{user?.DisplayName} Trying to insert a filter");
                _logger.LogInformation("Checking db for existing filter record that matches new record");
                Filter filterInDb = Get(filter.UserId.ToString(), filter.IsDefault, filter.KeyWord, ((int)filter.VoteType).ToString(), ((int)filter.FilterType).ToString());
                if (filterInDb != null) {
                    message = $"Filter with similar properties already exists";
                    throw new Exception(message);
                }
                _logger.LogInformation("No duplicates found, proceeding with insert");
                filter.CreatedDate = DateTime.Now;
                filter.CreatedBy = user?.DisplayName == null ? "unknown" : user.DisplayName;
                filter.User = _context.Users.Find(filter.UserId);
                try {
                    _context.Add(filter);
                    _context.SaveChanges();
                    message = $"Successfully added new filter with id {filter.Id}";
                    _logger.LogInformation(message);
                }
                catch (Exception e) {
                    isSuccess = false;
                    message = e.Message;
                    _logger.LogError(e, "Failed to insert");
                }
            }
            catch (Exception ex) {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex,"Failed to insert filter");
            }

            return filter;
        }
    }
}
