﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using System.Linq;

namespace DateNightDataServices
{
    public class ItemService
    {
        private readonly ILogger<ItemService> _logger;
        readonly DateNightPersistence.DataContext _context;


        public ItemService(DateNightPersistence.DataContext context, ILogger<ItemService> logger)
        {
            _context = context;
            _logger = logger;
        }
/*
        /// <summary>
        /// Method get an item record associated with passed in key
        /// </summary>
        /// <param name="key">item id</param>
        /// <returns>Item object</returns>
        public Item Get(string key)
        {
            _logger.LogInformation($"Get item using key {key}");
            //if key is null return null
            if (string.IsNullOrEmpty(key)) return null;

            //try to get the first item record where the key is the text
            Item item = _context.Items.FirstOrDefault(u => u.Text == key || u.ItemId == key);

            //try to parse key as long
            long keyLong;
            bool isValidLong = long.TryParse(key, out keyLong);

            //if item is null and key is a valid long, get first item record where the key is the id
            if (item == null && isValidLong) item = _context.Items.FirstOrDefault(u => u.Id.Equals(keyLong));

            //return item
            return item;
        }

        /// <summary>
        /// get a queryable list of item records
        /// </summary>
        /// <returns>queryable list of item records</returns>
        public IQueryable<Item> GetQueryable()
        {
            return _context.Items.AsQueryable();
        }

        /// <summary>
        /// Tries to insert a new item record into the database
        /// </summary>
        /// <param name="newItem">item object</param>
        /// <param name="user">user object inserting item</param>
        /// <param name="isSuccess">success flag referenced for call status</param>
        /// <param name="message">message string reference for call status</param>
        public void InsertNewItem(Item newItem, User user, ref bool isSuccess, ref string message)
        {
            try
            {
                //try to get item with same text
                Item itemInDb = Get(newItem.ItemId);
                //if null
                if (itemInDb == null)
                {
                    //set a few properties of item object and insert
                    newItem.CreatedDate = DateTime.Now;
                    newItem.CreatedBy = user.Email;
                    _context.Items.Add(newItem);
                    _context.SaveChanges();
                    message = "Inserted item successfully";
                    _logger.LogInformation($"{message}");
                }
                else
                {
                    //return false flag if not null
                    isSuccess = false;
                    message = "An item already exists";
                    _logger.LogError($"{message}");
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }
*/
    }

}
