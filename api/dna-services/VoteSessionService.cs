﻿using Microsoft.Extensions.Logging;
using System;
using DateNightPersistence.Models;
using System.Linq;
using DateNightPersistence.Enums;
using Newtonsoft.Json;

namespace DateNightDataServices
{
    public class VoteSessionService
    {
        private readonly ILogger<VoteSessionService> _logger;
        private readonly SignalRService _signalR;
        readonly DateNightPersistence.DataContext _context;
        private readonly NotificationService _notificationService;

        public VoteSessionService(DateNightPersistence.DataContext context, ILogger<VoteSessionService> logger, SignalRService signalRService, NotificationService notificationService)
        {
            _context = context;
            _logger = logger;
            _signalR = signalRService;
            _notificationService = notificationService;
        }

        public void ExpireVoteSessions(string updatedBy)
        {
            try
            {
                var sessions = _context.VoteSession
                    .Where(vs => (vs.voteSessionStatus != VoteSessionStatus.Cancelled && vs.voteSessionStatus != VoteSessionStatus.Complete && vs.voteSessionStatus != VoteSessionStatus.Expired)
                           && vs.ScheduledDateTime < DateTime.UtcNow).ToList();
                foreach (VoteSession session in sessions)
                {
                    bool isSuccess = true;
                    string message = "";
                    session.voteSessionStatus = VoteSessionStatus.Expired;
                    session.UpdatedDate = DateTime.Now;
                    session.UpdatedBy = updatedBy;
                    _context.SaveChanges();
                    Notification notificationForInviter = CreateNotificationObject(session, session.AUserId, NotificationType.VoteSessionExpired);
                    notificationForInviter.CreatedBy = updatedBy;
                    notificationForInviter.CreatedDate = DateTime.Now;
                    CheckAndUpdateMatchingNotification(notificationForInviter, ref isSuccess, ref message);
                    _notificationService.InsertNotification(notificationForInviter, ref isSuccess, ref message);
                    Notification notificationForInvitee = CreateNotificationObject(session, session.BUserId, NotificationType.VoteSessionExpired);
                    notificationForInviter.CreatedBy = updatedBy;
                    notificationForInviter.CreatedDate = DateTime.Now;
                    CheckAndUpdateMatchingNotification(notificationForInvitee, ref isSuccess, ref message);
                    _notificationService.InsertNotification(notificationForInvitee, ref isSuccess, ref message);
                }
                _logger.LogInformation($"Expired {sessions.Count} sessions");
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to expire sessions");
            }

        }

        /// <summary>
        /// Get vote session record associated with key
        /// </summary>
        /// <param name="key">identifier for object</param>
        /// <returns>vote session object</returns>
        public VoteSession Get(string key)
        {
            _logger.LogInformation($"getting vote session using key {key}");
            //if null return null
            if (string.IsNullOrEmpty(key)) return null;
            VoteSession voteSession = new VoteSession();//_context.VoteSession.FirstOrDefault(u => u.Title == key);
            //parse key as long
            long keyLong;
            bool isValidLong = long.TryParse(key, out keyLong);
            //if long, get first vote session with that id
            if (isValidLong) voteSession = _context.VoteSession.FirstOrDefault(u => u.Id.Equals(keyLong));

            //return vote session
            return voteSession;
        }

        /// <summary>
        /// Get queyable list of vote sessions
        /// </summary>
        /// <returns>list of vote sessions</returns>
        public IQueryable<VoteSession> GetQueryable()
        {
            _logger.LogInformation($"Getting vote session as queryable");
            return _context.VoteSession.AsQueryable();
        }

        /// <summary>
        /// set the vote session status of the session object
        /// </summary>
        /// <param name="voteSession">session object</param>
        /// <param name="addedBy">user object making changes</param>
        /// <param name="voteSessionStatus">new status</param>
        /// <param name="isSuccess">success flag</param>
        /// <param name="message">message reference</param>
        public void SetVoteSessionStatus(VoteSession voteSession, User addedBy, ref bool isSuccess, ref string message)
        {
            try
            {
                VoteSession sessionInDb = new VoteSession();
                //get vote session in db with the pairing of users
                _logger.LogInformation($"getting vote session records with pairing of {voteSession.AUserId} and {voteSession.BUserId}");
                IQueryable<VoteSession> voteSessionsInDb = _context.VoteSession.Where(
                    vs => (vs.AUserId.Equals(voteSession.AUserId) && vs.BUserId.Equals(voteSession.BUserId)) ||
                          (vs.AUserId.Equals(voteSession.BUserId) && vs.BUserId.Equals(voteSession.AUserId))).AsQueryable();
                //switch based on new status
                switch (voteSession.voteSessionStatus)
                {
                    //if not started
                    case (int)VoteSessionStatus.NotStarted:
                        _logger.LogInformation($"Creating new VoteSession: {voteSession.Title} ({voteSession.Id})");
                        //add new session
                        voteSession.CreatedBy = addedBy.DisplayName;
                        foreach (var vsf in voteSession.VoteSessionFilters)
                        {
                            if (vsf.Filter != null && vsf.Filter.Id != 0)
                            {
                                vsf.FilterId = vsf.Filter.Id;
                                vsf.Filter = null;
                            }
                        }
                        _context.VoteSession.Add(voteSession);
                        _context.SaveChanges();
                        message = "Vote session successfully created";
                        _logger.LogInformation($"{message}");
                        Notification notificationToSend = CreateNotificationObject(voteSession, addedBy.UserId, NotificationType.VoteSessionCreated);
                        notificationToSend.CreatedBy = addedBy.DisplayName;
                        notificationToSend.CreatedDate = DateTime.Now;
                        CheckAndUpdateMatchingNotification(notificationToSend, ref isSuccess, ref message);
                        _notificationService.InsertNotification(notificationToSend, ref isSuccess, ref message);
                        SendUpdateMessageToUsers(voteSession, message);
                        break;
                    //if any other status
                    default:
                        _logger.LogInformation($"Updating existing VoteSession: {voteSession.Title} ({voteSession.Id})");
                        //get first session object in queried list with id similar to object passed in
                        sessionInDb = voteSessionsInDb.FirstOrDefault(s => s.Id.Equals(voteSession.Id));
                        Notification notificationObj = CompareVoteSessionsAndCreateNotification(voteSession, sessionInDb, addedBy);
                        bool isDateTimeUpdated = sessionInDb.ScheduledDateTime != voteSession.ScheduledDateTime ? true : false;
                        _logger.LogInformation($"Check if vote session with id {voteSession.Id} exists");
                        //if not null
                        if (sessionInDb != null)
                        {
                            //set vote session status
                            sessionInDb.voteSessionStatus = voteSession.voteSessionStatus;
                            sessionInDb.OptionReduction = voteSession.OptionReduction;
                            sessionInDb.SearchTerms = voteSession.SearchTerms;
                            sessionInDb.OriginalSearchTerms = voteSession.OriginalSearchTerms;
                            sessionInDb.FinalizedItemIds = voteSession.FinalizedItemIds;
                            sessionInDb.UpdatedBy = addedBy.DisplayName;
                            sessionInDb.UpdatedDate = DateTime.Now;
                            sessionInDb.Location = voteSession.Location;
                            sessionInDb.ScheduledDateTime = voteSession.ScheduledDateTime;
                            sessionInDb.isDateTimeStrict = voteSession.isDateTimeStrict;
                            sessionInDb.Title = voteSession.Title;
                            //save changes
                            _context.SaveChanges();
                            message = "Vote Session successfully updated";
                            _logger.LogInformation($"{message}");
                            if (notificationObj != null)
                            {
                                notificationObj.CreatedBy = addedBy.DisplayName;
                                notificationObj.CreatedDate = DateTime.Now;
                                CheckAndUpdateMatchingNotification(notificationObj, ref isSuccess, ref message);
                                _notificationService.InsertNotification(notificationObj, ref isSuccess, ref message);
                            }
                            if (isDateTimeUpdated)
                                sendDateTimeUpdatedNotification(voteSession, addedBy, ref isSuccess, ref message);
                            SendUpdateMessageToUsers(voteSession, message);
                        }
                        else
                        {
                            //return false
                            isSuccess = false;
                            message = "Vote Session does not exist";
                            _logger.LogError($"{message}");
                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
                _logger.LogError(ex, "Failed to save");
            }
        }

        public void CancelVoteSessionsBetweenUsers(Guid? userFriendId, Guid userId, User updater, ref bool isSuccess, ref string message)
        {
            
            
            User userA = _context.Users.Find(userId);
            User userB = _context.Users.Find(userFriendId);
            var voteSessionsInDb = _context.VoteSession.Where(vs => ((vs.AUserId.Equals(userId) && vs.BUserId.Equals(userFriendId))
                                                                || (vs.AUserId.Equals(userFriendId) && vs.BUserId.Equals(userId))) && vs.voteSessionStatus != VoteSessionStatus.Cancelled).ToList();
            foreach (VoteSession voteSession in voteSessionsInDb) {
                voteSession.voteSessionStatus = VoteSessionStatus.Cancelled;
                voteSession.UpdatedBy = updater.DisplayName;
                voteSession.UpdatedDate = DateTime.Now;
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception e) {
                isSuccess = false;
                message = $"Failed to cancel vote sessions between users {userA.DisplayName} and {userB.DisplayName}";
                _logger.LogError(e, $"Failed to cancel: {message}");
            }
        }

        private void CheckAndUpdateMatchingNotification(Notification notificationToSend, ref bool isSuccess, ref string message)
        {
            Notification notificationInDb = _notificationService.IsExistingNotDismissedNotification(notificationToSend);
            if (notificationInDb != null)
            {
                _notificationService.UpdateNotificationStatus(notificationInDb.Id.ToString(), NotificationStatus.Dismissed, ref isSuccess, ref message);
            }
        }

        private void sendDateTimeUpdatedNotification(VoteSession voteSession, User addedBy, ref bool isSuccess, ref string message)
        {
            Notification AUserNotification = CreateNotificationObject(voteSession, voteSession.AUserId, NotificationType.VoteSessionDateTimeUpdated);
            AUserNotification.CreatedBy = addedBy.DisplayName;
            AUserNotification.CreatedDate = DateTime.Now;
            CheckAndUpdateMatchingNotification(AUserNotification, ref isSuccess, ref message);
            _notificationService.InsertNotification(AUserNotification, ref isSuccess, ref message);
            Notification BUserNotification = CreateNotificationObject(voteSession, voteSession.BUserId, NotificationType.VoteSessionDateTimeUpdated);
            BUserNotification.CreatedBy = addedBy.DisplayName;
            BUserNotification.CreatedDate = DateTime.Now;
            CheckAndUpdateMatchingNotification(BUserNotification, ref isSuccess, ref message);
            _notificationService.InsertNotification(BUserNotification, ref isSuccess, ref message);
        }

        private Notification CompareVoteSessionsAndCreateNotification(VoteSession voteSession, VoteSession sessionInDb, User addedBy)
        {
            Notification notificationObj =null;

            bool usersTurn = voteSession.voteSessionStatus != sessionInDb.voteSessionStatus &&
                (voteSession.voteSessionStatus == VoteSessionStatus.AUserVotes || voteSession.voteSessionStatus == VoteSessionStatus.BUserVotes);
            bool finalized = voteSession.voteSessionStatus != sessionInDb.voteSessionStatus && voteSession.voteSessionStatus == VoteSessionStatus.Complete;

            if (usersTurn) {
                _logger.LogInformation($"Creating YourTurn notification. {{ addedBy.UserId: {addedBy.UserId}, voteSessionDto.Status: {voteSession.voteSessionStatus}, voteSessionDbo.Status: {sessionInDb.voteSessionStatus} }}");
                notificationObj = CreateNotificationObject(voteSession, addedBy.UserId, NotificationType.VoteSessionYourTurn);
            }
            else if (finalized) {
                _logger.LogInformation($"Creating Finalized notification. {{ addedBy.UserId: {addedBy.UserId}, voteSessionDto.Status: {voteSession.voteSessionStatus}, voteSessionDbo.Status: {sessionInDb.voteSessionStatus} }}");
                notificationObj = CreateNotificationObject(voteSession, addedBy.UserId, NotificationType.VoteSessionFinalized);
            }
            else if (voteSession.voteSessionStatus == VoteSessionStatus.Cancelled)
            {
                _logger.LogInformation($"Creating Cancelled notification. {{ addedBy.UserId: {addedBy.UserId}, voteSessionDto.Status: {voteSession.voteSessionStatus}, voteSessionDbo.Status: {sessionInDb.voteSessionStatus} }}");
                notificationObj = CreateNotificationObject(voteSession, addedBy.UserId, NotificationType.VoteSessionCancelled);
            }
            return notificationObj;
        }

        private Notification CreateNotificationObject(VoteSession voteSession, Guid userId, NotificationType notificationType)
        {
            User currentUser = _context.Users.Find(userId);
            User otherUser = userId == voteSession.AUserId ? voteSession.UserB : voteSession.UserA;

            if (currentUser == null || otherUser == null)
            { 
                return null;
            }

            Notification notificationObj = new Notification();
            notificationObj.TableType = TableType.VoteSession;
            notificationObj.RecordId = voteSession.Id.ToString();
            notificationObj.NotificationStatus = NotificationStatus.NotViewed;
            notificationObj.NotificationType = notificationType;

            switch (notificationType) {
                case NotificationType.VoteSessionCreated:
                    _logger.LogInformation($"Creating VoteSessionCreated notification for {otherUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = otherUser.UserId;
                    notificationObj.User = otherUser;
                    notificationObj.Message = $"{currentUser.DisplayName} has started a hangout with you";
                    break;
                case NotificationType.VoteSessionYourTurn:
                    _logger.LogInformation($"Creating VoteSessionYourTurn notification for {otherUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = otherUser.UserId;
                    notificationObj.User = otherUser;
                    notificationObj.Message = $"It is now your turn to vote in the hangout with {currentUser.DisplayName}";
                    break;
                case NotificationType.VoteSessionFinalized:
                    _logger.LogInformation($"Creating VoteSessionFinalized notification for {otherUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = otherUser.UserId;
                    notificationObj.User = otherUser;
                    notificationObj.Message = $"Hangout has been finalized by {currentUser.DisplayName}";
                    break;
                case NotificationType.VoteSessionDateTimeUpdated:
                    _logger.LogInformation($"Creating VoteSessionDateTimeUpdated notification for {currentUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = currentUser.UserId;
                    notificationObj.User = currentUser;
                    notificationObj.Message = $"The scheduled date and time for the hangout between you and {otherUser.DisplayName} has been updated";
                    break;
                case NotificationType.VoteSessionExpired:
                    _logger.LogInformation($"Creating VoteSessionExpired notification for {currentUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = currentUser.UserId;
                    notificationObj.User = currentUser;
                    notificationObj.Message = $"The hangout between you and {otherUser.DisplayName} has expired";
                    break;

                case NotificationType.VoteSessionCancelled:
                    _logger.LogInformation($"Creating VoteSessionCancelled notification for {otherUser.DisplayName}. PartnerName: {otherUser.DisplayName}");
                    notificationObj.UserId = otherUser.UserId;
                    notificationObj.User = otherUser;
                    notificationObj.Message = $"The hangout between you and {currentUser.DisplayName} has been cancelled";
                    break;
            }
            return notificationObj;
        }

        private void SendUpdateMessageToUsers(VoteSession voteSession, string message)
        {
            // This doesn't really need to be awaited, so turning
            // these outer methods async isn't a high priority
            #pragma warning disable CS4014

            if (voteSession.voteSessionStatus == VoteSessionStatus.AUserVotes)
            {
                _signalR.SendMessageAsync(
                    hubName: "VoteSession",
                    method: "Update",
                    userId: voteSession.AUserId.ToString(),
                    message: message);
            } 
            else if (voteSession.voteSessionStatus == VoteSessionStatus.BUserVotes)
            {
                _signalR.SendMessageAsync(
                    hubName: "VoteSession",
                    method: "Update",
                    userId: voteSession.BUserId.ToString(),
                    message: message);
            }
            else
            {
                // Handle status changes that can't be gleaned from current
                // status by sending message to both users
                _signalR.SendMessageAsync(
                    hubName: "VoteSession",
                    method: "Update",
                    userId: voteSession.AUserId.ToString(),
                    message: message);

                _signalR.SendMessageAsync(
                    hubName: "VoteSession",
                    method: "Update",
                    userId: voteSession.BUserId.ToString(),
                    message: message);
            }
            
            #pragma warning restore CS4014

            _logger.LogInformation($"Update SignalR message(s) sent for VoteSession id#{voteSession.Id}");
        }
    }
}
